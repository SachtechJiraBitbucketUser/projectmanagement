$('.projectDate .input-group.date').datepicker({
    startView: 1,
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    format: "dd-mm-yyyy"
});
$(".js-example-basic-multiple").select2({placeholder: 'Select an option'});
function showEmployeeList(emp_role) {
    var url = "../../User/getEmployeeList";
    $(".loaderr").show();
    $.post(url,{"emp_role":"All"},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            var empData = data.data;
            var employeeData = "";
            if(emp_role == "Manager"){
                var devEmpId = $("#projectTeamDeveloper").val();
                for(var i = 0; i < empData.length; i++){
                    employeeData = employeeData + '<div class="col-sm-3"><div class="form-group"><input type="radio" ' +
                        ' name="managerName" id="' + empData[i].emp_name + '" value="'+empData[i].emp_id+'">&nbsp;' +
                        '<label class="capital-first-letter">' + empData[i].emp_name+'</label></div></div> ';
                }
                $("#employeeManagerListBox .row").html(employeeData+'<div class="col-sm-12 "><button type="button"' +
                    'class="btn btn-default pull-right" style="margin-left:10px" id="selectManager" onclick=addEmployee("hideBox")>Cancel' +
                    '</button><button type="button" ' +
                    'class="btn btn-primary pull-right" id="selectManager" onclick=addEmployee("Manager")>Submit</button>' +
                    '</div>');
                $("#top_title").html("Select Manager");
                $("#createEmployeeBox").hide();
                $("#employeeDeveloperListBox").hide();
                $("#employeeManagerListBox").show();
                $('input:radio[name="managerName"]').filter('[value="'+$("#projectManager").val()+'"]').attr('checked', true);
            }
            else if(emp_role == "Developer"){
                var projManId = $("#projectManager").val();
                for(var i = 0; i < empData.length; i++){
                    employeeData = employeeData + '<div class="col-sm-3"><div class="form-group"><input type="checkbox" ' +
                    'name="developerName" class="developerName" id="'+empData[i].emp_name+'" value="'+empData[i].emp_id+'">' +
                        '&nbsp;<label class="capital-first-letter">' + empData[i].emp_name+'</label></div></div> ';
                }
                $("#employeeDeveloperListBox .row").html(employeeData+'<div class="col-sm-12 "><button type="button"' +
                    ' class="btn btn-default pull-right" style="margin-left:10px" id="selectManager" ' +
                    'onclick=addEmployee("hideBox")>Cancel</button>' +
                    '<button type="button" class="btn btn-primary pull-right" id="selectManager" ' +
                    'onclick=addEmployee("Developer")>Submit</button></div>');
                $("#top_title").html("Select Developer");
                $("#createEmployeeBox").hide();
                $("#employeeManagerListBox").hide();
                $("#employeeDeveloperListBox").show();
                var projTeamVal = $("#projectTeamDeveloper").val();
                if(projTeamVal.length>0){
                    for (var p =0; p < projTeamVal.length; p++){
                        $('input:checkbox[name="developerName"][value="'+projTeamVal[p]+'"]').prop('checked',true);
                    }
                }
            }
        }
        else{
            $(".loaderr").hide();
            $(".modal-body").html('<div class="row" style="padding: 0px">' +
                '<label>'+Message+'</label></div>');
            $("#myModal").modal("show");
            setTimeout(function () {
                $("#myModal").modal("hide");
            },3000);
        }
    });
}
function addEmployee(emp_type) {
    /*for particular emp_type  > Manager Developer */
    if(emp_type == "Manager"){
        var managerId = $('input[name="managerName"]:checked').val();
        var managerName = $('input[name="managerName"]:checked').attr('id');
        $("#projectManager").html("<option value='"+managerId+"'>"+managerName+"</option>");
    }
    if(emp_type == "Developer"){
        var empDevTeam = [];
        $(".developerName:checked").each(function(){
            empDevTeam.push($(this).val())
        });
        $("#projectTeamDeveloper").val(empDevTeam).trigger('change');
    }
    $("#createEmployeeBox").show();
    $("#employeeManagerListBox").hide();
    $("#employeeDeveloperListBox").hide();
    $("#top_title").html("");
}
function getFieldData(emp_id,actionType,proj_id) {
    var url = "../../CreateProject/getFieldsData";
    $(".loaderr").show();
    $.post(url,{"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var selectOption = "<option value=''>Select</option>";
        var typeOptions = "";
        var statusOptions = "";
        var empDevOptions = "";
        if (Status) {
            var typeData = data.typeData;
            var statusData = data.statusData;
            var empDevData = data.empDevData;
            var projMaxNum = data.projMaxNum;
            if(projMaxNum.length < 1){
                console.log("project max data not found");
                $("#projectId").val(1);
            }
            else {
                projMaxNum = projMaxNum;
                if(projMaxNum == "" || projMaxNum == null){
                    $("#projectId").val(1);
                }
                else {
                    $("#projectId").val(parseInt(projMaxNum) + 1);
                }
            }
            if(typeData.length < 1){
                console.log("Type data not found");
            }
            else {
                for (var j = 0; j < typeData.length; j++) {
                    typeOptions = typeOptions + "<option value='" + typeData[j].type_id + "'>" + typeData[j].type_name + "</option>";
                }
                $("#projectType").html(selectOption+ typeOptions);
            }
            if(empDevData.length < 1){
                console.log("Employee Developer data not found");
            }
            else {
                for (var n=0; n<empDevData.length; n++) {
                    empDevOptions = empDevOptions + "<option value='" + empDevData[n].emp_id + "'>" + empDevData[n].emp_name + "</option>";
                }
                $("#projectTeamDeveloper").html(selectOption+ empDevOptions);
                $(".js-example-basic-multiple").select2({placeholder: 'Select Developer'});
            }
            if(statusData.length < 1){
                console.log("Status data not found");
            }
            else {
                for (var j = 0; j < statusData.length; j++) {
                    statusOptions = statusOptions + "<option value='" + statusData[j].status_id + "'>" + statusData[j].status_title + "</option>";
                }
                $("#projectStatus").html(selectOption+ statusOptions);
            }
            if(actionType == "editProject"){
                setTimeout(function () {
                    getParticularProject(proj_id,emp_id);
                },1000);
            }
            else {
                $(".loaderr").hide();
            }
        }
        else {
            console.log(Status + " " + Message);
            $(".loaderr").hide();
        }

    });
}
function projTypeChange() {
    var projectStatus = $("#amountType").val();
    if(projectStatus == "fixedPrice"){
        $(".timeMaterialBox").hide();
        $(".fixedPriceBox").show();
    }
    else if(projectStatus == "timeMaterial"){
        $(".timeMaterialBox").show();
        $(".fixedPriceBox").hide();
    }
}
var mileNum = 1;
$(function () {
    $("#milestoneAdd").bind("click", function () {
        var div = $("<div />");
        mileNum = mileNum+1;
        div.html(GetDynamicTextBox(""));
        $("#milestoneBoxes").append(div);
    });
    $("#btnGet").bind("click", function () {
        var values = "";
        $("input[name=DynamicTextBox]").each(function () {
            values += $(this).val() + "\n";
        });
        alert(values);
    });
    $("body").on("click", ".removeMilestone", function () {
        $(this).closest("div").remove();
    });
});
function GetDynamicTextBox(value) {
    return '<div class="col-sm-3"><i class="fa fa-times pull-right removeMilestone"></i><div class="form-group">' +
        '<label>Project Milestone</label><textarea class="form-control projectMilestone" name = "DynamicTextBox"' +
        'id="projectMilestone" placeholder="Milestone"></textarea></div></div>';
}
function creatProject(emp_type) {
    var baseUrl = $("#baseUrl").val();
    var proj_emp_id            =  $("#proj_emp_id").val();
    var projectId              =  $("#projectId").val();
    var projectType            =  $("#projectType").val();
    var projectName            =  $("#projectName").val();
    var projectStatus          =  $("#projectStatus").val();
    var projectManager         =  $("#projectManager").val();
    var projectTeamDeveloper   =  $("#projectTeamDeveloper").val();
    var projectComment         =  $("#projectComment").val();
    var startDate              =  $("#startDate").val();
    var deadlineDate           =  $("#delieveryDate").val();
    var closingDate            =  $("#closingDate").val();
    var milestoneValues = [];
    var amountType = ""; var totalProjCost= ""; var totalProjMember = ""; var totalProjHours = "";
    $("textarea[name='DynamicTextBox']").each(function () {
        milestoneValues.push($(this).val());
    });
    projectTeamDeveloper = projectTeamDeveloper.toString();
    if(projectType == "" || projectName == "" || projectStatus == "" || projectManager == ""
    ||  startDate == "" || projectComment == "" || deadlineDate == ""){
        $("#top_title").text("please fill required fields data");
        $("#top_title").css("color","red");
        return false;
    }
    if(emp_type == "admin"){
        amountType =  $("#amountType").val();
        if(amountType == ""){
            $("#top_title").text("please select amount type");
            $("#top_title").css("color","red");
            return false;
        }
        if(amountType == "fixedPrice"){
             totalProjCost = $("#totalProjCost").val();
        }
        else if(amountType == "timeMaterial"){
             totalProjMember = $("#totalProjMember").val();
             totalProjHours  = $("#totalProjHours").val();
             totalProjCost = $("#timeMaterialProjectCost").val();
        }
    }
    var currentDate     =  moment().format('DD-MM-YYYY');
    $("#top_title").text("");
    $(".loaderr").show();
    var url = "../../CreateProject/createProject";
    $.post(url,{"emp_id":proj_emp_id,"projectId":projectId,"projectType":projectType,"projectName":projectName,"projectStatus":projectStatus,
        "projectManager":projectManager, "projectTeamDeveloper":projectTeamDeveloper,"projectComment":projectComment,
        "startDate":startDate,"deadlineDate":deadlineDate,"closingDate":closingDate,"amountType":amountType,
        "milestoneValues":milestoneValues,"totalProjCost":totalProjCost,"totalProjMember":totalProjMember,
        "totalProjHours":totalProjHours,"currentDate":currentDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){
            $(".loaderr").hide();
            $(".modal-body").html('<div class="row" style="padding: 0px">' +
                '<label style="color: green">'+Message+'</label></div>');
            $("#myModal").modal("show");
            $("#top_title").html("");
            setTimeout(function () {
                $("#myModal").modal("hide");
                window.location = baseUrl;
            },3000);
        }
        else{
            $(".loaderr").hide();
            $(".modal-body").html('<div class="row" style="padding: 0px">' +
                '<label style="color: red">'+Message+'</label></div>');
            $("#myModal").modal("show");
            $("#top_title").html("");
            setTimeout(function () {
                $("#myModal").modal("hide");
            },3000);
        }
    });
}
function updateProject(proj_id,emp_id,emp_type) {
    var proj_emp_id            =  $("#proj_emp_id").val();
    var projectId              =  $("#projectId").val();
    var projectType            =  $("#projectType").val();
    var projectName            =  $("#projectName").val();
    var projectStatus          =  $("#projectStatus").val();
    var projectManager         =  $("#projectManager").val();
    var projectTeamDeveloper   =  $("#projectTeamDeveloper").val();
    var startDate              =  $("#startDate").val();
    var deadlineDate           =  $("#delieveryDate").val();
    var closingDate            =  $("#closingDate").val();
    var amountType             =  $("#amountType").val();
    projectTeamDeveloper = projectTeamDeveloper.toString();
    var amountType = ""; var totalProjCost= ""; var totalProjMember = ""; var totalProjHours = "";
    if(projectType == "" || projectName == "" || projectStatus == "" || projectManager == ""
        || startDate == "" || deadlineDate == ""){
        $("#top_title").text("please fill required fields data");
        $("#top_title").css("color","red");
        return false;
    }
    if(emp_type == "admin"){
        amountType =  $("#amountType").val();
        if(amountType == ""){
            $("#top_title").text("please select amount type");
            $("#top_title").css("color","red");
            return false;
        }
        if(amountType == "fixedPrice"){
            totalProjCost = $("#totalProjCost").val();
        }
        else if(amountType == "timeMaterial"){
            totalProjMember = $("#totalProjMember").val();
            totalProjHours  = $("#totalProjHours").val();
            totalProjCost  = $("#timeMaterialProjectCost").val();
        }
    }
    var currentDate     =  moment().format('DD-MM-YYYY');
    $("#top_title").text("");
    $(".loaderr").show();
    var url = "../../CreateProject/updateProject";
    $.post(url,{"proj_id":proj_id,"emp_id":emp_id,"projectId":projectId,"projectType":projectType,"projectName":projectName,
        "projectStatus":projectStatus,"projectManager":projectManager, "projectTeamDeveloper":projectTeamDeveloper,
        "startDate":startDate,"deadlineDate":deadlineDate,"closingDate":closingDate,"amountType":amountType,
        "totalProjCost":totalProjCost,"totalProjMember":totalProjMember,
        "totalProjHours":totalProjHours,"currentDate":currentDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){
            $(".loaderr").hide();
            $(".modal-body").html('<div class="row" style="padding: 0px">' +
                '<label style="color: green">'+Message+'</label></div>');
            $("#myModal").modal("show");
            $("#top_title").html("");
            setTimeout(function () {
                $("#myModal").modal("hide");
                location.reload();
            },3000);
        }
        else{
            $(".loaderr").hide();
            $(".modal-body").html('<div class="row" style="padding: 0px">' +
                '<label style="color: red">'+Message+'</label></div>');
            $("#myModal").modal("show");
            $("#top_title").html("");
            setTimeout(function () {
                $("#myModal").modal("hide");
            },3000);
        }
    });
}
function getParticularProject(proj_id,emp_id) {
    var url = "../../Dashboard/getParticularProject";
    $(".loaderr").show();
    $.post(url,{"proj_id":proj_id,"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            var projectData = data.projectData;
            $("#projectId").val(projectData[0].projectNumber);
            $("#projectType").val(projectData[0].proj_type);
            $("#projectName").val(projectData[0].proj_name);
            $("#projectStatus").val(projectData[0].proj_status);
            $("#projectManager").html('<option value="">Project Manager</option>' +
                '<option value="'+projectData[0].proj_manager+'">'+projectData[0].proj_manager_name+'</option>');
            $("#projectManager").val(projectData[0].proj_manager);
            var proj_developers =  projectData[0].proj_developer;
            if(proj_developers.indexOf(",") > 0 ){
                proj_developers = proj_developers.split(",");
                $("#projectTeamDeveloper").val(proj_developers).trigger('change');
            }
            else {
                $("#projectTeamDeveloper").val(proj_developers).trigger('change');
            }
            $("#projectComment").val(projectData[0].proj_type);
            $("#startDate").val(projectData[0].proj_start_date);
            $("#delieveryDate").val(projectData[0].proj_dead_line);
            $("#closingDate").val(projectData[0].proj_closing_date);
            if(data.emp_type == "admin"){
                $("#updateProjectBtn").html('<button class="btn btn-md btn-primary pull-right" style="" ' +
                    'onclick=updateProject("'+proj_id+'","'+emp_id+'","'+data.emp_type+'"); type="button"><strong>Update</strong></button>');
                $("#amountType").val(projectData[0].proj_amount_type);
                if(projectData[0].proj_amount_type == "fixedPrice"){
                    $(".fixedPriceBox").show();
                    $(".timeMaterialBox").hide();
                    $("#totalProjCost").val(projectData[0].proj_total_amount);
                }
                else if(projectData[0].proj_amount_type == "timeMaterial"){
                    $(".fixedPriceBox").hide();
                    $(".timeMaterialBox").show();
                    $("#totalProjMember").val(projectData[0].proj_total_emp);
                    $("#totalProjHours").val(projectData[0].proj_hourly_rate);
                    $("#timeMaterialProjectCost").val(projectData[0].proj_total_amount);
                }
                /*fixedPrice timeMaterial*/
            }
            $(".loaderr").hide();
        }
        else{
            $(".loaderr").hide();
            alert(Status +" " +Message);
        }
    });
}

