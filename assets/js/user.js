/**
 * Created by SachTech on 26-12-2017.
 */
$('.only-letters').bind('keyup blur',function(){
    var node = $(this);
    node.val(node.val().replace(/[^a-zA-Z]/g,'') ); }
);
$('.only-numbers').bind('keyup blur',function(){
    var node = $(this);
    node.val(node.val().replace(/[^0-9]/g,'') ); }
);
/*load library when click to update for select state and city start here*/
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var users = [];
var typeOptions = "";
var desigOptions = "";
function getFieldsData(emp_id) {
    var url = "../User/getFieldsData";
    $.post(url,{"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var selectOption = "<option value=''>Select</option>";

        if (Status) {
            $(".loaderr").hide();
            var typeData = data.typeData;
            var desig_data = data.desig_data;
            if(typeData.length < 1){
                console.log("Type data not found");
            }
            for (var j = 0; j < typeData.length; j++) {
                typeOptions = typeOptions + "<option value='" + typeData[j].type_id + "'>" + typeData[j].type_name + "</option>";
            }
            if(desig_data.length < 1){
                console.log("Type data not found");
            }
            for (var j = 0; j < desig_data.length; j++) {
                desigOptions = desigOptions + "<option value='" + desig_data[j].desig_id + "'>" + desig_data[j].desig_title + "</option>";
            }
        }
        else {
            console.log(Status + " " + Message);
        }
    });
}
function onLoginPress() {
    var url = "../user/login";
    var email_id = $("#email_id").val();
    var password = $("#password").val();

    if(email_id === '' || password === '') {
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
            '<div class="col-md-12" style="margin-top: 2%"><p>Please enter all fields</p></div><div class="col-md-12">' +
            '</div>');
        $("#showMessage").modal("show");
        return false;
    }
    if(!validateEmail(email_id)){
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
            '<div class="col-md-12" style="margin-top: 2%"><p>Please enter valid email-address</p></div><div class="col-md-12">' +
            '</div>');
        $("#showMessage").modal("show");
        return false;
    }
    $.post(url,{email_id:email_id,password:password},function (data) {
        console.log('response -- '+JSON.stringify(data));
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
                '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
                '<div class="col-md-12" style="margin-top: 2%"><p>'+message+'</p></div><div class="col-md-12">' +
                '</div>');
            $("#showMessage").modal("show");
            return false;
        }
        else{
            window.location = "../";
        }
    });
}
function onDismissDialog(id) {
    $("#"+id).modal("hide");
}
function getAllUsers() {
    var url = "../user/users_data";
    var th_ = '<thead><tr><th>#</th><th>Name</th><th>Email</th><th>Designation</th>' +
              '<th style="text-align: center">Operations</th></tr></thead>';
    var td_ = '<tbody>';
    $.post(url,function (data) {
       var status = data.Status;
       var message = data.Message;
       if(status) {
           users = data.data;
           for(var i=0;i<users.length;i++) {
             var obj = users[i];
             if(obj.emp_type == "admin"){
                 td_ = td_+'<tr><td>'+(i+1)+'</td><td class="capital-first-letter">'+obj.emp_name+'</td>' +
                     '<td>'+obj.emp_email+'</td><td class="capital-first-letter">'+obj.emp_designation_name
                     +'</td><td style="text-align: center"><button class="btn-primary btn-sm" ' +
                     'onclick="editEmployee('+i+');"><i class="fa fa-pencil"></i></button></td></tr>';
             }
             else{
                 td_ = td_+'<tr><td>'+(i+1)+'</td><td class="capital-first-letter">'+obj.emp_name+'</td>' +
                     '<td>'+obj.emp_email+'</td><td class="capital-first-letter">'+obj.emp_designation_name
                     +'</td><td style="text-align: center"><button class="btn-primary btn-sm" onclick="editEmployee('+i+');">' +
                     '<i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button class="btn-danger btn-sm" ' +
                     'onclick="deleteEmployee('+i+');"><i class="fa fa-trash-o"></i></button></td></tr>';
             }

           }
           $("#userData").html(th_+td_+'</tbody>');
           $("#userData").dataTable({destroy:true});
           $(".loaderr").hide();
       }
       else{
           $(".loaderr").hide();
       }
    });
    $("#userData").html(th_+td_+'</tbody>');
    $("#userData").dataTable({destroy:true});
}
function createEmployee() {
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Create Employee</label>' +
        '</div> <div class="col-md-12" style="margin-top: 2%"> <div class="col-md-4"><label> Name </label><input ' +
        'type="text" placeholder="name" id="emp_name" class="form-control"/></div><div class="col-md-4">' +
        '<label>Email</label><input type="text" placeholder="email" id="emp_email" class="form-control"/></div>' +
        '<div class="col-md-4"><label>Password</label><input type="text" placeholder="password"' +
        ' id="emp_password" class="form-control"/></div></div><div class="col-md-12" style="margin-top: 2%">' +
        '<div class="col-md-4"><label>Designation</label><select id="emp_desg" class="form-control">' +
        '<option value="">Select</option>' +desigOptions+   '</select>' +
        '</div><div class="col-md-4"><label>Status</label><select id="emp_status" class="form-control"><option value="A">Active</option><option value="D">Disabled</option>' +
        '</select></div></div></div><div class="col-md-12" style="margin-top: 2%;margin-left: 14px;"><button class="btn' +
        ' btn-primary btn-sm" onclick="onCreateEmployee();"><i class="fa fa-floppy-o"></i> Save</button>&nbsp;&nbsp;' +
        '<button class="btn btn-default btn-sm" onclick=onDismissDialog("myModal");><i class="fa fa-trash-o"></i> Cancel' +
        '</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}
function onCreateEmployee() {
    var emp_name = $("#emp_name").val();
    var emp_email = $("#emp_email").val();
    emp_email = emp_email.toLowerCase();
    var emp_password = $("#emp_password").val();
    var emp_desg = $("#emp_desg").val();
    /*var emp_role = $("#emp_role").val();*/
    var emp_status = $("#emp_status").val();
    var emp_type = "user";
    var created_at = moment().format('DD-MM-YYYY hh:mm');
    $(".loaderr").show();
    if(emp_name === '' || emp_email === '' || emp_password === '') {
        $("#error").html("Please enter all fields");
        $("#error").css("color","red");
        return false;
    }
    var emp_data  = {emp_name:emp_name,emp_email:emp_email,emp_password:emp_password,emp_desg:emp_desg,
        emp_status:emp_status,emp_type:emp_type,emp_created_at:created_at};
    var url = '../user/user_create';
    $.post(url,emp_data,function (data) {
     var status = data.Status;
         var message = data.Message;
         if(status) {
             $("#myModal").modal("hide");
             getAllUsers();
         }
         else{
             $(".loaderr").hide();
             $("#error").html(message);
             $("#error").css("color","red");
         }
    });
}
function editEmployee(index) {
    var emp_id = users[index].emp_id;
    var emp_name = users[index].emp_name;
    var emp_email = users[index].emp_email;
    var emp_desg = users[index].emp_designation;
    /*var emp_role = users[index].emp_role;*/
    var emp_status = users[index].emp_status;
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Update</label></div> ' +
        '<div class="col-md-12" style="margin-top: 2%"><div class="col-md-4"><label>Name</label><input type="text"' +
        ' placeholder="Enter employee name" id="emp_name" class="form-control" value="'+emp_name+'"></div>' +
        '<div class="col-md-4"><label>Email</label><input type="text" placeholder="Enter employee email" ' +
        'id="emp_email" class="form-control" value='+emp_email+' disabled="disabled"></div>' +
        '<div class="col-md-4"><label>Designation</label>' +
        '<select id="emp_desg" class="form-control"><option value="">Select</option>'+desigOptions+'</select>' +
        '</div></div>' +
        '<div class="col-md-12" style="margin-top: 2%"><div class="col-md-4"><label>Status</label><select id="emp_status"' +
        ' class="form-control"><option value="A">Active</option><option value="D">Disabled</option>' +
        '</select></div></div></div>' +
        '<div class="col-md-12" style="margin-top: 2%;margin-left: 14px;"><button class="btn btn-primary btn-sm" ' +
        'onclick="onUpdateEmployee('+emp_id+');"><i class="fa fa-floppy-o"></i> Save</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick=onDismissDialog("myModal");><i class="fa ' +
        'fa-trash-o"></i> Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
    $("#emp_desg").val(emp_desg);
    /*$("#emp_role").val(emp_role);*/
    $("#emp_status").val(emp_status);


}
function onUpdateEmployee(id) {
    var emp_name = $("#emp_name").val();
   /* var emp_role = $("#emp_role").val();*/
    var emp_email = $("#emp_email").val();
    var emp_desg = $("#emp_desg").val();
    var emp_status = $("#emp_status").val();
    if(emp_name === '' || emp_email === '' ) {
        $("#error").html("Please enter all fields");
        $("#error").css("color","red");
        return false;
    }
    $(".loaderr").show();
    var emp_data  = {emp_name:emp_name,emp_email:emp_email,emp_desg:emp_desg,emp_status:emp_status,emp_id:id};
    var url = '../user/user_update';
    $.post(url,emp_data,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllUsers();
        }
        else{
            $(".loaderr").hide();
            $("#error").html(message);
            $("#error").css("color","red");
        }
    });
}
function deleteEmployee(index) {
    var emp_id = users[index].emp_id;
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Delete</label></div> <div class="col-md-12" style="margin-top: 2%;">' +
        '<p>Would you like to remove employee (y/n) ?</p></div><div class="col-md-12" style="margin-top: 2%;padding-top: 2%;border-top:1px #eee solid"><button class="btn btn-danger btn-sm" onclick="onDeleteEmployee('+emp_id+');"><i class="fa fa-floppy-o"></i> Remove</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick=onDismissDialog("myModal");><i class="fa fa-trash-o"></i> Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}

function onDeleteEmployee(emp_id) {
    var url = '../user/user_remove';
    $(".loaderr").show();
    $.post(url,{emp_id:emp_id},function (data) {
        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllUsers();
        }
        else{
            $(".loaderr").hide();
            $("#error").html(message);
            $("#error").css("color","red");
        }

    });
}
function getPartUser(emp_id) {
    var base_url = $("#baseUrl").val();
    var url = "../user/getPartUser";
    $(".loaderr").show();
    $.post(url,{"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            var userInfo = data.userInfo;
            var userImgPath = base_url+"assets/img/userImg/"+ userInfo[0].emp_img;
            $("#user_name_field").val(jsStringfirst(userInfo[0].emp_name));
            $(".user_name").html(jsStringfirst(userInfo[0].emp_name));

            $("#user_email_field").val(userInfo[0].emp_email);
            $(".user_email").html(userInfo[0].emp_email);

            $(".user_designation").html(jsStringfirst(userInfo[0].designation));
            $("#user_designation_field").val(jsStringfirst(userInfo[0].emp_designation));

            $(".user_mobile").html(userInfo[0].emp_mobile);
            $("#user_mobile_field").val(userInfo[0].emp_mobile);

            $("#user_message_field").val(jsStringfirst(userInfo[0].emp_message));
            $(".user_message").html(jsStringfirst(userInfo[0].emp_message));
            if(userInfo[0].emp_img != ""){
                $(".oldUserImg").attr("src", userImgPath);
                $(".userHeaderProfile").attr("src", userImgPath);
            }
            $(".userImgBtn").css("display","none");
        }
        else{
            $(".loaderr").hide();
            $("#main_row1").html(Message);
            $("#myModal").modal("show");
        }

    });
}
var oldUserImagechanged = "no";
function readURL(input,targetClass) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.'+targetClass).attr('src', e.target.result);
            if(targetClass == "oldUserImg"){
                oldUserImagechanged = "yes";
                $(".userImgBtn").show();
            }else{
                oldUserImagechanged = "yes";
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function updateUserInfo(emp_id) {
    var user_name_field         = $("#user_name_field").val();
    var user_email_field        = $("#user_email_field").val();
    var user_designation_field  = $("#user_designation_field").val();
    var user_mobile_field       = $("#user_mobile_field").val();
    var user_message_field      = $("#user_message_field").val();
    if(user_name_field == "" || user_email_field == "" || user_designation_field == ""){
        $("#main_row1").html("<label style='color: red'>please fill required fields</label>");
        $("#myModal").modal("show");
        return false;
    }
    user_email_field = user_email_field.toLowerCase(user_email_field);
    if(validateEmail(user_email_field)){
        var url = "../user/userInfoUpdate";
        $(".loaderr").show();
        $.post(url,{"emp_id":emp_id,"user_name":user_name_field,"user_email":user_email_field,"user_designation":
        user_designation_field,"user_mobile":user_mobile_field,"user_message":user_message_field},function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status){
                $(".loaderr").hide();
                $("#main_row1").html("<label style='color: green'>"+Message+"</label>");
                $("#myModal").modal("show");
                setTimeout(function () {
                    //getPartUser(emp_id);
                    location.reload();
                    $("#myModal").modal("hide");
                },3000);
            }
            else{
                $(".loaderr").hide();
                $("#main_row1").html("<label style='color: red'>"+Message+"</label>");
                $("#myModal").modal("show");
                setTimeout(function () {
                    $("#myModal").modal("hide");
                },3000);
            }
        });
    }
    else {
        $("#main_row1").html("<label style='color: red'>Please fill valid email address !!</label>");
        $("#myModal").modal("show");
        return false;
    }
}
function changeUserImage(emp_id) {
    var user_image_file = $("#user_image_file").val();
    var data = new FormData();
    if (user_image_file !== "") {
        var file_user_image_file = document.getElementById('user_image_file');
        var user_image_file_ext = user_image_file.split(".");
        user_image_file_ext = user_image_file_ext[user_image_file_ext.length - 1];
        user_image_file_ext = user_image_file_ext.toLowerCase();
        if (user_image_file_ext == "jpg" || user_image_file_ext == "png" || user_image_file_ext == "gif"
            || user_image_file_ext == "jpeg") {
            data.append('user_image_file', file_user_image_file.files[0]);
            data.append('emp_id', emp_id);
        }
        else {
            $("#main_row1").html("<label style='color: red'>Image File Should Be JPG, PNG, GIF Formats Only</label>");
            $("#myModal").modal("show");
            setTimeout(function () {
                $("#myModal").modal("hide");
            },3000);
            $("#subbtn").attr("disabled", false);
            return false;
        }
        $(".loaderr").show();
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                var response = $.parseJSON(request.response);
                var Status = response.Status;
                var Message = response.Message;
                if(Status) {
                    $(".loaderr").hide();
                    $("#main_row1").html("<label style='color: green'>"+Message+"</label>");
                    $("#myModal").modal("show");
                    setTimeout(function () {
                        getPartUser(emp_id);
                        $("#myModal").modal("hide");
                    },3000);
                }
                else {
                    $(".loaderr").hide();
                    $("#main_row1").html("<label style='color: red'>Something Went Wrong !!! Please do the Same Littlebit Later....</label>");
                    $("#myModal").modal("show");
                    setTimeout(function () {
                        $("#myModal").modal("hide");
                    },3000);
                }
            }
        };
        request.open('POST', '../user/userImageUpdate');
        request.send(data);
    }
    else{
        $("#main_row1").html("<label style='color: red'>Please select image first !!</label>");
        $("#myModal").modal("show");
        setTimeout(function () {
            $("#myModal").modal("hide");
        },3000);
    }
}
function sendOtp() {
    var emp_email = $("#emp_email").val();
    if(emp_email == ""){
        $("#main_row1").html("<label style='color: red'> </label>");
        $("#myModal").modal("show");
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div><div class="col-md-12" style="margin-top: 2%;color: red">' +
            '<p>Please fill email address.</p></div><div class="col-md-12"></div>');
        $("#showMessage").modal("show");
        return false;
    }
    if(!validateEmail(emp_email)){
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
            '<div class="col-md-12" style="margin-top: 2%;color: red"><p>Please fill valid email address !!</p></div><div class="col-md-12">' +
            '</div>');
        $("#showMessage").modal("show");
        return false;
    }
    emp_email = emp_email.toLowerCase();
    $(".loaderr").show();
    var url = "../user/sendOtp";
    $.post(url,{"emp_email":emp_email},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Succes</label>' +
                '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
                '<div class="col-md-12" style="margin-top: 2%;color: green"><p>'+Message+'</p></div><div class="col-md-12">' +
                '</div>');
            $("#showMessage").modal("show");

            $(".sendOtpBtn").hide();
            $(".forgotPasswordBox").show();
            $(".otpValidBtn").show();
        }
        else {
            $(".loaderr").hide();
            $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
                '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
                '<div class="col-md-12" style="margin-top: 2%;color: red"><p>'+Message+'</p></div><div class="col-md-12">' +
                '</div>');
            $("#showMessage").modal("show");;
        }
    });
}
function otpValidUser() {
    var baseUrl =  $("#baseUrl").val();
    var emp_email = $("#emp_email").val();
    var otp = $("#otp").val();
    var newPass = $("#newPass").val();
    var confirmPass = $("#confirmPass").val();
    if(emp_email == "" || otp == "" || newPass == "" || confirmPass == ""){
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
            '<div class="col-md-12" style="margin-top: 2%;color: red"><p>Please fill all fields data !!</p></div><div class="col-md-12">' +
            '</div>');
        $("#showMessage").modal("show");
        return false;
    }
    if(!validateEmail(emp_email)){
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
            '<div class="col-md-12" style="margin-top: 2%;color: red"><p>Please fill valid email address !!</p></div><div class="col-md-12">' +
            '</div>');
        $("#showMessage").modal("show");
        return false;
    }
    if( newPass != confirmPass ){
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
            '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
            '<div class="col-md-12" style="margin-top: 2%;color: red"><p>Password not match !!</p></div><div class="col-md-12">' +
            '</div>');
        $("#showMessage").modal("show");
        return false;
    }
    emp_email = emp_email.toLowerCase();
    $(".loaderr").show();
    var url = "../user/otpValidUser";
    $.post(url,{"emp_email":emp_email,"otp":otp,"newPass":newPass},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Success</label>' +
                '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
                '<div class="col-md-12" style="margin-top: 2%;color: green"><p>'+Message+'</p></div><div class="col-md-12">' +
                '</div>');
            $("#showMessage").modal("show");
            setTimeout(function () {
                window.location = baseUrl+"user/";
            },2000);
        }
        else {
            $(".loaderr").hide();
            $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label>' +
                '<i class="fa fa-times-circle pull-right" data-dismiss="modal" style="cursor: pointer"></i></div>' +
                '<div class="col-md-12" style="margin-top: 2%;color: red"><p>'+Message+'</p></div><div class="col-md-12">' +
                '</div>');
            $("#showMessage").modal("show");
        }
    });
}
