/**
 * Created by SachTech on 28-12-2017.
 */
var privlegeData = [];
var employeeData = [];

window.onload = function () {
    getAllPrivlege();
};

function getAllPrivlege() {
    $(".privlegeData").show();
    $(".userPrivlegeBox").hide();
    var url = 'privlege/users_data';
    var th_ = '<thead><tr><th>#</th><th>Name</th><th>Designation</th><th>Email</th>' +
        '<th>Privlege</th></tr></thead>';
    var td_='<tbody>';
    $.get(url,function (data) {
        //console.log('data --- ' + JSON.stringify(data));
        var status = data.Status;
        var message = data.Message;
        if (status) {
            employeeData = data.data;
            for (var i = 0; i < employeeData.length; i++) {
                td_ = td_ + '<tr><td>' + (i + 1) + '</td><td class="capital-first-letter">' + employeeData[i].emp_name + '</td>' +
                    '<td class="capital-first-letter">' + employeeData[i].designation + '</td><td>' + employeeData[i].emp_email + '</td>' +
                    '<td><span style="text-decoration: underline;cursor: pointer" onclick=privlege('+i+');>Privlege</span></td></tr>';
            }
            $("#typeData").html(th_ + td_ + '</tbody>');
        }
        $("#privlegeData").html(th_ + td_ + '</tbody>');
        $("#privlegeData").dataTable({destroy:true});
    });
}

function privlege(index) {
    var menuList = "";
    var emp_id = employeeData[index].emp_id;
    var userPrivlegeData = "";
    var url = "privlege/get_privlege";
    $.post(url,{"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            userPrivlegeData = data.userData;
            var data = data.data;
            for(var k = 0; k < data.length; k++){
                menuList = menuList + "<div class='col-md-4'><input type='checkbox'  value='"+data[k].id+"' " +
                    " class='privlegeCheckBox'>&nbsp; <label> "+data[k].menu_name+"</label></div>";
            }
        }
        else {
            alert(Status+ " "+Message);
        }
        /*$("#main_row1").html("<div class='col-md-12 no-padding' style='margin-top: 20px;margin-bottom: 20px;'>" +
            "<p class='privlegeHeading'>By Page Privlege </p>" +menuList +  " </div>" +
            "<div class='col-md-12 no-padding'" +
            " style='margin-top: 20px;margin-bottom: 20px;'><p class='privlegeHeading'>Employee Privlege " +
            "</p><div class='col-md-4'><input type='checkbox'  value='create_emp' " +
            " class='privlegeUserCheck'>&nbsp; <label> Create Employee</label></div>" +
            "<div class='col-md-4'><input type='checkbox'  value='edit_emp' class='privlegeUserCheck'>&nbsp; " +
            "<label> Edit Employee</label></div><div class='col-md-4'> <input type='checkbox'  value='delete_emp' " +
            " class='privlegeUserCheck'>&nbsp; <label> Delete Employee</label></div></div>" +
            "<div class='col-md-12 no-padding'" +
            " style='margin-top: 20px;margin-bottom: 20px;'><p class='privlegeHeading'>Project Privlege " +
            "</p><div class='col-md-4'><input type='checkbox'  value='create_proj' " +
            " class='privlegeUserCheck'>&nbsp; <label> Create Project</label></div>" +
            "<div class='col-md-4'><input type='checkbox'  value='edit_proj' class='privlegeUserCheck'>&nbsp; " +
            "<label> Edit Project</label></div><div class='col-md-4'> <input type='checkbox'  value='delete_proj' " +
            " class='privlegeUserCheck'>&nbsp; <label> Delete Project</label></div></div>" +
            "<div class='col-md-12 no-padding'><button class='btn btn-primary btn-sm' " +
            "onclick=onAddPrivlege("+index+");><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm'" +
            " data-dismiss='modal' style='margin-left: 1%'><i class='fa fa-trash-o'></i> Cancel </button>" +
            "<label id='error' style='margin-left: 1%'></label></div>");
        $(".modal-dialog").addClass("modal-lg");
        $("#myModal").modal("show");*/
        $(".privlegeData").hide();
        $(".userPrivlegeBox").show();
        $("#userPrivlegeBox").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Privlege" +
        " to "+userPrivlegeData[0].emp_name+"</label><input type='button' class='pull-right btn-primary btn' value='Back'" +
            "onclick='getAllPrivlege()'></div><div class='col-md-12 no-padding' style='margin-top: 20px;'>" +
        "<p class='privlegeHeading'>By Page Privlege </p>" +menuList +  " </div>" +

        "<div class='col-md-12 no-padding'><button class='btn btn-primary btn-sm' " +
        "onclick=onAddPrivlege("+index+");><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm'" +
        " onclick='getAllPrivlege()' style='margin-left: 1%'><i class='fa fa-trash-o'></i> Cancel </button>" +
        "<label id='error' style='margin-left: 1%'></label></div>");
        var privlegeValues = employeeData[index].emp_menu_privlege;
        if(privlegeValues.indexOf(",") > 0){
            privlegeValues = privlegeValues.split(",");
            for(var k = 0; k< privlegeValues.length; k++){
                $('.privlegeCheckBox[value="'+privlegeValues[k]+'"]').prop('checked', true);
            }
        }
        else {
            $('.privlegeCheckBox[value="'+privlegeValues+'"]').prop('checked', true);
        }
    });
}

function onAddPrivlege(index) {
    var emp_id = employeeData[index].emp_id;
    var privlegeCheckBox = "";
    $(".privlegeCheckBox:checked").each(function(){
        privlegeCheckBox = privlegeCheckBox + $(this).val() + ",";
    });
    privlegeCheckBox = privlegeCheckBox.substr(0,privlegeCheckBox.length-1);
    var created_at = moment().format("DD-MM-YYYY");
    var url = 'privlege/createPrivlege';
    var type_data = {"emp_id":emp_id,"privlegeValue":privlegeCheckBox,"created_at":created_at};
    $.post(url,type_data,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#error").html(message);
        $("#error").css("color","green");
        setTimeout(function () {
            getAllPrivlege();
        },1000);

    });
}