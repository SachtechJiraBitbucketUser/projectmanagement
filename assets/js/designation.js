/**
 * Created by SachTech on 28-12-2017.
 */
var DesignationData = [];

window.onload = function () {
    getAllDesignation();
};
function getAllDesignation() {
    var baseUrl = $("#baseUrl").val();
    var url = baseUrl+'designation/getAllDesignation';
    var th_ = '<thead><tr><th>#</th><th>Designation</th><th>Created AT</th><th>Updated AT</th>' +
        '<th style="text-align: center">Action</th></tr></thead>';
    var td_='<tbody>';
    $.get(url,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(status) {
            /*for delete */
            DesignationData = data.data;
            for(var i=0;i<DesignationData.length;i++) {
                td_ = td_+'<tr><td>'+(i+1)+'</td><td class="capital-first-letter">'+DesignationData[i].desig_title+'</td>' +
                    '<td>'+DesignationData[i].desi_created_at+'</td><td>'+DesignationData[i].desig_updated_at+'</td>' +
                    '<td style="text-align: center"><button class="btn btn-primary btn-sm" onclick="editDesignation('+i+')">' +
                    '<i class="fa fa-pencil"></i></button>&nbsp;&nbsp;<button class="btn btn-danger btn-sm" ' +
                    'onclick=deleteDesignation("'+i+'");><i class="fa fa-trash-o"></i></button></td></tr>';
            }
            $("#designationData").html(th_+td_+'</tbody>');
        }
        $("#designationData").html(th_+td_+'</tbody>');
        $("#designationData").dataTable({destroy:true});
    });
}
function createDesignation() {
    $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Create</label>" +
        "</div><div class='col-md-12 no-padding' style='margin-top: 2%'><div class='col-md-6 no-padding' " +
        "style='padding-left:1% !important; '><label>Designation</label><input type='text' placeholder=' employee designation'" +
        " id='emp_designation' class='form-control'></div></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<button class='btn btn-primary btn-sm' onclick='onCreateDesignation();'><i class='fa fa-floppy-o'></i> Save</button>" +
        "<button class='btn btn-default btn-sm' data-dismiss='modal' style='margin-left: 1%'>" +
        "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
    $("#myModal").modal("show");
}
function onCreateDesignation() {
    var baseUrl = $("#baseUrl").val();
    var emp_designation = $("#emp_designation").val();
    var created_at = moment().format("DD-MM-YYYY");
    if(emp_designation === '') {
        $("#error").html("Please enter designation..");
        $("#error").css("color","red");
        return false;
    }
    var url = baseUrl+'designation/createDesignation';
    var type_data = {"emp_designation":emp_designation,"created_at":created_at};
    $.post(url,type_data,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#myModal").modal("hide");
        getAllDesignation();

    });
}

function editDesignation(index) {
    var designation_id = DesignationData[index].desig_id;
    var designation_title = DesignationData[index].desig_title;
    $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Update Designation" +
        "</label></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<div class='col-md-6 no-padding' style='padding-left:1% !important; '>" +
        "<label>Designation</label><input type='text' id='desig_title' class='form-control' placeholder='project status'" +
        "value='" + designation_title + "'></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<button class='btn btn-primary btn-sm' onclick=onUpdateDesignation('" + designation_id + "');>" +
        "<i class='fa fa-floppy-o'></i> Save</button><button" +
        " class='btn btn-default btn-sm' data-dismiss='modal'  style='margin-left: 1%'>" +
        "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
    $("#myModal").modal("show");
}

function onUpdateDesignation(id) {
    var baseUrl = $("#baseUrl").val();
    var desig_title = $("#desig_title").val();
    if( desig_title === '') {
        $("#error").html("Please enter Designation..");
        $("#error").css("color","red");
        return false;
    }
    var url = baseUrl+'designation/designation_update';
    $.post(url,{'desig_id':id,'desi_title':desig_title},function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#myModal").modal("hide");
        getAllDesignation();
    });

}
function deleteDesignation(index) {
    var desig_id = DesignationData[index].desig_id;
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Delete</label></div>' +
        ' <div class="col-md-12" style="margin-top: 2%;"><p>Would you like to remove status (y/n) ?</p></div>' +
        '<div class="col-md-12" style="margin-top: 2%;padding-top: 2%;border-top:1px #eee solid"><button ' +
        'class="btn btn-danger btn-sm" onclick="onDeleteDesignation('+desig_id+');"><i class="fa fa-floppy-o"></i> Remove</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" data-dismiss="modal" ><i class="fa fa-trash-o"></i> ' +
        'Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}


function onDeleteDesignation(desig_id) {
    var baseUrl = $("#baseUrl").val();
    var url = baseUrl+'designation/removeDestination';
    $.post(url,{"desig_id":desig_id},function (data) {

        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllDesignation();
        }
        else{
            $("#error").html(message);
            $("#error").css("color","red");
        }

    });
}