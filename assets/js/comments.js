/*get  project start here*/

function getProjects(proj_id,emp_id) {
    var url = "../../Dashboard/getParticularProject";
    var table = '<thead><tr><th>#</th><th>Project</th><th>ProjectType</th>' +
        '<th>Manager</th><th>Team Leader</th><th>Start Date </th><th>Dead Line</th><th>Add On</th>' +
        ' </tr></thead><tbody>';
    var tbody = "";
    $(".loaderr").show();
    $.post(url,{"proj_id":proj_id,"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var table ='<thead><tr><th>#</th><th>Project</th><th>Type</th><th>Manager</th><th>Start Date </th>' +
            '<th>Dead Line</th><th>Closing Date</th><th>Milestone</th></tr></thead><tbody>';
        var tbody = "";
        if(Status){
            var j = 0;
            var projectData = data.projectData;
            for(var i=0; i < projectData.length; i++){
                j = i+1;
                tbody = tbody + '<tr><td>'+ j+'</td><td class="capital-first-letter">'+projectData[i].proj_name+'<small></small></td>' +
                    ' <td class="capital-first-letter">'+projectData[i].proj_type_name+'</td><td class="capital-first-letter">'+projectData[i].proj_manager_name+'</td>' +
                    '<td>' +projectData[i].proj_start_date+
                    '</td><td>'+projectData[i].proj_dead_line+'</td><td>'+projectData[i].proj_closing_date+
                    '</td><td><span class="projectComm" onclick=projectMilestone("'+projectData[i].proj_id+'");>' +
                    ' Milestone <i class="fa fa-eye"></i></span></td></tr>';
            }
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable({destroy:true});
        }
        else{
            $(".loaderr").hide();
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable({destroy:true});
        }
    });
    getComments(proj_id);
}
/*get  project end here*/

/*get conversation start here*/
function getComments(proj_id) {
    var baseUrl = $("#baseUrl").val();
    var proj_emp_id = $("#proj_emp_id").val();
    var url = "../../comments/getComments";
    $.post(url,{"proj_id":proj_id,"comm_emp_id":proj_emp_id},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        var commentData = "";
        if(Status){
            var data = data.data;var emp_img = baseUrl+"assets/img/userImg/dummy_user.png";
            for(var i = 0; i < data.length; i++){
                if(data[i].emp_img != "" ){
                    emp_img = baseUrl+"assets/img/userImg/"+data[i].emp_img;
                }
                if(proj_emp_id != data[i].comm_emp_id){
                    commentData = commentData + '<div class="chat-element"><a href="#" class="pull-left"><img alt="image" ' +
                        'class="img-circle" src="'+emp_img+'"></a>' +
                        '<div class="media-body "><small class="pull-right text-navy"></small><p style="margin: 0px;" class="capital-first-letter"><strong>'+data[i].emp_name
                        +'</strong></p><p class="m-b-xs">'+data[i].comm_text+'</p><small class="text-muted">'+data[i].comm_created_at
                        +'</small></div></div>';
                }
                else{
                    commentData = commentData+ '<div class="chat-element right"><a href="#" class="pull-right"><img alt="image" ' +
                        'class="img-circle" src="'+emp_img+'"></a><div class="media-body text-right "><small class="pull-left"></small>' +
                        '<p style="margin: 0px;" class="capital-first-letter"><strong>'+data[i].emp_name+'</strong></p><p class="m-b-xs">'+data[i].comm_text+'</p><small class="text-muted">' +
                         data[i].comm_created_at+'</small>' +
                        '</div></div>';
                }
            }
            $(".loaderr").hide();
        }
        else{
            commentData = "Comments not found";
            $(".loaderr").hide();
        }
        $("#commentBox").html(commentData);
    });
}
/*get comments end here*/
/* add comments start here*/

function addComment(proj_id) {
    var messageUser = $("#messageUser").val();
    var proj_emp_id = $("#proj_emp_id").val();
    var currentDate     =  moment().format('DD-MM-YYYY hh:mm:ss');
    var url = "../../comments/addComments";
    $.post(url,{"proj_id":proj_id,"proj_emp_id":proj_emp_id,"messageUser":messageUser,"currentDate":currentDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status){
            //alert(Status + " "+Message);
        }
        else{
            alert(Status + " "+Message);
        }
    });
    $("#messageUser").val("");
    getComments(proj_id);
}
/* add comments end here*/
function projectMilestone(proj_id) {
    window.location = "../../milestone/view/"+proj_id;
}