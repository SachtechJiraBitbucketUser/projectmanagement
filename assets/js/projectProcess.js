/* array of all data < -- */
var projectData = [];
/* // > array of all data  */
/*get all project start here*/
function getProjectCount() {
    var url ="dashboard/getProjectCount";
    $.post(url,function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if(Status){
            $("#top_total_projects").html(data.totalProj);
            $("#top_hold_projects").html(data.onHoldProj);
            $("#top_projects_running").html(data.assignedProj);
            $("#top_projects_live").html(data.liveStabilizationProj);
            $("#top_projects_closed").html(data.liveClosedProj);
            $("#top_projects_archived").html(data.archivedProj);
            $("#top_projects_new").html(data.newProj);
        }
    });
}
function getAllProjects(emp_id) {
    var url = "dashboard/getAllProjects";
    var table = '<thead><tr><th>#</th><th>Project</th><th>ProjectType</th>' +
        '<th>Manager</th><th>Start Date </th><th>Dead Line</th><th>Add On</th><th>Action</th>' +
        ' </tr></thead><tbody>';
    var tbody = "";
    $(".loaderr").show();
    $.post(url,{"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var table ='<thead><tr><th>#</th><th>Project</th><th>Type</th><th>Manager</th>' +
            '<th>Start Date </th><th>Dead Line</th><th>Closing Date</th><th>Status</th><th>Milestone</th><th>Comments</th>' +
            '<th>Action</th></tr></thead><tbody>';
        var tbody = "";
        var createProject = ""; var deleteProjectValue = "";
        if(Status == "Success"){
            $(".loaderr").hide();
            projectData = data.AllProject;
            var j = 0;
            for(var i=0; i < projectData.length; i++){
                j = i+1;
                if(data.emp_type == "admin"){
                    createProject = '<a href="createProject/view/" class="btn btn-primary">Create Project</a>';
                    deleteProjectValue = '<a onclick=confirmDeleteProject("'+projectData[i].proj_id+'","'+emp_id+'")>' +
                        '<button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash text-navy ' +
                        'project-fa"></i></button></a>&nbsp;&nbsp;';
                }
                tbody = tbody + '<tr><td>'+ j+'</td><td class="capital-first-letter"><a class="projectNameText" ' +
                    'style="text-decoration: underline" href="createProject/editProject/'+projectData[i].proj_id+'">' +
                    '<span>'+projectData[i].proj_name+'</span></a></td>' +
                    ' <td>'+projectData[i].proj_type_name+'</td><td class="capital-first-letter">'+projectData[i].proj_manager_name+'</td>' +
                    '<td>' +projectData[i].proj_start_date+ '</td><td>'+projectData[i].proj_dead_line+'</td>' +
                    '<td>'+projectData[i].proj_closing_date+'</td><td>'+projectData[i].status_name+'</td>' +
                    '<td><span class="projectComm" onclick=projectMilestone("'+projectData[i].proj_id+'");>' +
                    ' Milestone <i class="fa fa-eye"></i></span></td>' +
                    '<td><span class="projectComm" onclick=projectComments("'+projectData[i].proj_id+'")> ' +
                    'Comments <i class="fa fa-eye"></i></span>' +
                    '</td><td>'+deleteProjectValue+'<a href="createProject/editProject/'+projectData[i].proj_id+'">' +
                    '<button class="btn' +
                    ' btn-primary btn-sm" type="button"><i class="fa fa-edit text-navy project-fa">' +
                    '</i></button></a></td></tr>';
            }
            $("#createProjectBox").html(createProject);
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable({destroy:true});
        }
        else {
            $(".loaderr").hide();
            if(data.emp_type == "admin"){
                createProject = '<a href="createProject/view/" class="btn btn-primary">Create Project</a>';
                $("#createProjectBox").html(createProject);
            }
        }
        $("#projectData").html(table+tbody+"</tbody>");
        $("#projectData").dataTable({destroy:true});
    });
}
function getStatusProjects(emp_id,statusValue) {
    var baseUrl = $("#baseUrl").val();
    if(statusValue == '1'){
        statusValue = "all";
    }
    var url = baseUrl+"dashboard/getAllProjects";
    var table = '<thead><tr><th>#</th><th>Project</th><th>ProjectType</th>' +
        '<th>Manager</th><th>Start Date </th><th>Dead Line</th><th>Add On</th><th>Action</th>' +
        ' </tr></thead><tbody>';
    var tbody = "";
    $(".loaderr").show();
    $.post(url,{"emp_id":emp_id,"status_value":statusValue},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var table ='<thead><tr><th>#</th><th>Project</th><th>Type</th><th>Manager</th>' +
            '<th>Start Date </th><th>Dead Line</th><th>Closing Date</th><th>Status</th><th>Milestone</th><th>Comments</th>' +
            '<th>Action</th></tr></thead><tbody>';
        var tbody = "";
        var createProject = ""; var deleteProjectValue = "";
        if(Status == "Success"){
            $(".loaderr").hide();
            projectData = data.AllProject;
            var j = 0;
            for(var i=0; i < projectData.length; i++){
                j = i+1;
                if(data.emp_type == "admin"){
                    createProject = '<a href="'+baseUrl+'createProject/view/" class="btn btn-primary">Create Project</a>';
                    deleteProjectValue = '<a onclick=confirmDeleteProject("'+projectData[i].proj_id+'","'+emp_id+'")>' +
                        '<button class="btn btn-danger btn-sm" type="button"><i class="fa fa-trash text-navy ' +
                        'project-fa"></i></button></a>&nbsp;&nbsp;';
                }
                tbody = tbody + '<tr><td>'+ j+'</td><td class="capital-first-letter"><a class="projectNameText" ' +
                    ' style="text-decoration: underline" href="'+baseUrl+'createProject/editProject/'+projectData[i].proj_id+'">' +
                    '<span>'+projectData[i].proj_name+'</span></a></td>' +
                    ' <td>'+projectData[i].proj_type_name+'</td><td class="capital-first-letter">'+projectData[i].proj_manager_name+'</td>' +
                    '<td>' +projectData[i].proj_start_date+ '</td><td>'+projectData[i].proj_dead_line+'</td>' +
                    '<td>'+projectData[i].proj_closing_date+'</td><td>'+projectData[i].status_name+'</td></td>' +
                    '<td><span class="projectComm" onclick=projectMilestone("'+projectData[i].proj_id+'");>' +
                    ' Milestone <i class="fa fa-eye"></i></span></td>' +
                    '<td><span class="projectComm" onclick=projectComments("'+projectData[i].proj_id+'")> ' +
                    'Comments <i class="fa fa-eye"></i></span>' +
                    '</td><td>'+deleteProjectValue+'<a href="'+baseUrl+'createProject/editProject/'+projectData[i].proj_id+'">' +
                    '<button class="btn' +
                    ' btn-primary btn-sm" type="button"><i class="fa fa-edit text-navy project-fa">' +
                    '</i></button></a></td></tr>';
            }
            $("#createProjectBox").html(createProject);
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable({destroy:true});
        }
        else {
            $(".loaderr").hide();
            if(data.emp_type == "admin"){
                createProject = '<a href="'+baseUrl+'createProject/view/" class="btn btn-primary">Create Project</a>';
                $("#createProjectBox").html(createProject);
            }
        }
        $("#projectData").html(table+tbody+"</tbody>");
        $("#projectData").dataTable({destroy:true});
    });
}
/*get all project end here*/

/*for delete project start here*/
function confirmDeleteProject(proj_id,emp_id) {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Delete Project</h3>' +
        '<p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<label>Are you sure want to delete this project ?</label></div></div></div>' +
        '<hr><div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<button data-dismiss="modal" class="btn btn-md btn-primary pull-right" style="margin: 0 5px" ' +
        ' type="button"><strong>Cancel</strong></button><button class="btn btn-md btn-danger pull-right "' +
        'style="margin: 0 5px"  id="projectDltBtn" type="button" onclick=projectDelete("'+proj_id+'","'+emp_id+'");><strong>Submit</strong></button></div></div></div>');
    $("#myModal").modal("show");
}
function projectDelete(proj_id,emp_id) {
    var baseUrl = $("#baseUrl").val();
    var url = baseUrl+"dashboard/deleteProject";
    $("#myModal #projectDltBtn").prop("disabled",true);
    $.post(url,{"proj_id":proj_id},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){
            getProjectCount();
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","green");
            setTimeout(function () {
                $("#myModal").modal("hide");
                //getAllProjects(emp_id);
                location.reload();
            },2000);
        }
        else{
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","red");
            $("#myModal #projectDltBtn").prop("disabled",false);
        }
    });
}
/*for delete project end   here*/
/*edit projcet strat here*/
function editProject(proj_id,index) {
    var projectOption = "";
    var employeeOption = "";
    var url = "dashboard/get_project_employee";
    $.post(url,function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if (Status) {
            var projectTypeData = data.allProjectType;
            var employeeTypeData = data.allEmployee;
            for (var j = 0; j < projectTypeData.length; j++) {
                projectOption = projectOption + "<option value='" + projectTypeData[j].type_id + "'>" + projectTypeData[j].proj_type_name + "</option>";

            }
            for (var j = 0; j < employeeTypeData.length; j++) {
                employeeOption = employeeOption + "<option value='" + employeeTypeData[j].emp_id + "'>" + employeeTypeData[j].emp_name + "</option>";

            }
        }
        else {
            console.log(Status + " " + Message);
        }
       console.log(JSON.stringify(projectData[index]));
        $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Update Project</h3>' +
            '<p style="color: red" id="err_message"></p></div><hr>' +
            '<div class="row">' +
            '<div class="col-sm-4"><div class="form-group"><label>Project Name</label> <input type="text" ' +
            'placeholder="Project Name" class="form-control" id="projectName" value="' + projectData[index].proj_name + '"></div></div>' +
            '<div class="col-sm-4"><div class="form-group"><label>Project Type</label><select class="form-control" ' +
            'id="projectType"><option>Select Project Type</option>'+projectOption+'</select>' +
            '</div></div>' +
            '<div class="col-sm-4"><div class="form-group"><label>Project Manager</label><select class="form-control"' +
            ' id="projectManager"><option>Project Manager</option>'+employeeOption+'</select>' +
            '</div></div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-sm-4"><div class="form-group"><label>Project Team Leader</label><select class="form-control"' +
            ' id="projectLeader"><option>Project Team Leader</option>'+employeeOption+'</select></div></div>' +
            '<div class="col-sm-4">' +
            '<div class="form-group projectDate"><label>Project Start Date</label>' +
            '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
            '<input id="startDate" type="text" class="form-control" placeholder="start date" ' +
            'value="' + projectData[index].proj_start_date + '"></div></div>' +
            '</div>' +
            '<div class="col-sm-4"><div class="form-group projectDate"><label>Project Deadline Date</label>' +
            '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
            '<input id="deadlineDate" type="text" class="form-control" placeholder="deadline date"' +
            ' value="' + projectData[index].proj_dead_line + '"></div></div></div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-sm-4"><div class="form-group projectDate" ><label>Project Closing Date</label>' +
            '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
            '<input id="closingDate" type="text" class="form-control" placeholder="closing date" ' +
            'value="' + projectData[index].proj_closing_date + '"></div>' +
            '</div></div>' +
            '<div class="col-sm-8"><div class="form-group"><label>Project Team</label><div>' +
            '<select class="js-example-basic-multiple prjectTeam form-control" name="states[]"' +
            ' multiple="multiple" id="projectTeam">' +employeeOption+  ' </select>' +
            '</div></div>' +
            '</div>' +
            '<div class="col-sm-12"><button class="btn btn-md btn-primary pull-right" style="margin-top: 25px" ' +
            ' type="button" onclick=projectEdit("' + proj_id + '")>' +
            '<strong>Submit</strong>' +
            '</button>' +
            '</div></div>');
        $("#myModal").modal("show");

        $('#projectType option[value="' + projectData[index].proj_type + '"]').attr("selected", "selected");
        $('#projectManager option[value="' + projectData[index].proj_manager + '"]').attr("selected", "selected");
        $('#projectLeader option[value="' + projectData[index].proj_team_leader + '"]').attr("selected", "selected");
        $('.projectDate .input-group.date').datepicker({
            startView: 1,
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            autoclose: true,
            format: "dd-mm-yyyy"
        });
        $(".js-example-basic-multiple").select2({placeholder: 'Select an option'});
        var projectTeamId = projectData[index].projectTeam;
        var teamArray = [];
        for(var k=0; k< projectTeamId.length; k++){
            teamArray.push(projectTeamId[k].emp_id);
        }
        $(".js-example-basic-multiple").val(teamArray).trigger('change');
    });
}
function projectEdit(proj_id) {
    var projectName     =  $("#myModal #projectName").val();
    var projectType     =  $("#myModal #projectType").val();
    var projectManager  =  $("#myModal #projectManager").val();
    var projectLeader   =  $("#myModal #projectLeader").val();
    var projectTeam     =  $("#myModal #projectTeam").val();
    var startDate       =  $("#myModal #startDate").val();
    var deadlineDate    =  $("#myModal #deadlineDate").val();
    var closingDate     =  $("#myModal #closingDate").val();
    projectTeam = projectTeam.toString();
    if(projectName == "" || projectType == ""){
        $("#myModal #err_message").html("please fill required fields");
        $("#myModal #err_message").css("color","red");
        return false;
    }
    var url = "dashboard/updateProject";
    $.post(url,{"proj_id":proj_id,"projectName":projectName,"projectType":projectType,"projectManager":projectManager,
        "projectLeader":projectLeader,"projectTeam":projectTeam,"startDate":startDate,"deadlineDate":deadlineDate,
        "closingDate":closingDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){

            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","green");
            setTimeout(function () {
                $("#myModal").modal("hide");
                //getAllProjects();
                location.reload();
            },2000);
        }
        else{
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","red");
        }
    });
}
/*edit projcet end  here*/
function projectMilestone(proj_id) {
    var baseUrl = $("#baseUrl").val();
    window.location = baseUrl+"milestone/view/"+proj_id;
}
function projectComments(proj_id) {
    var baseUrl = $("#baseUrl").val();
    window.location = baseUrl+"comments/view/"+proj_id;
}