function getProjects(proj_id,emp_id) {
    var url = "../../Dashboard/getParticularProject";
    var table = '<thead><tr><th>#</th><th>Project</th><th>ProjectType</th>' +
        '<th>Manager</th><th>Team Leader</th><th>Start Date </th><th>Dead Line</th><th>Add On</th>' +
        ' </tr></thead><tbody>';
    var tbody = "";
    $(".loaderr").show();
    $.post(url,{"proj_id":proj_id,"emp_id":emp_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var user_type = "";
        var table ='<thead><tr><th>#</th><th>Project</th><th>Type</th><th>Manager</th><th>Start Date </th>' +
            '<th>Dead Line</th><th>Closing Date</th><th>Milestone</th></tr></thead><tbody>';
        var tbody = "";
        if(Status){
            var j = 0;
            var projectData = data.projectData;
            user_type = data.emp_type;
            if(user_type == "admin"){
                $("#createMilestone").html('<a class="btn btn-primary" onclick=createMilestone("'+proj_id+'","'+user_type+'")>Create Milestone</a>');
            }
            for(var i=0; i < projectData.length; i++){
                j = i+1;
                tbody = tbody + '<tr><td>'+ j+'</td><td class="capital-first-letter">'+projectData[i].proj_name+'<small></small></td>' +
                    ' <td class="capital-first-letter">'+projectData[i].proj_type_name+'</td><td class="capital-first-letter">'+
                     projectData[i].proj_manager_name+'</td>' +
                    '<td>' +projectData[i].proj_start_date+
                    '</td><td>'+projectData[i].proj_dead_line+'</td><td>'+projectData[i].proj_closing_date+
                    '</td><td><span class="projectComm" onclick=projectComments("'+projectData[i].proj_id+'");>' +
                    ' Comments <i class="fa fa-eye"></i></span></td></tr>';
            }
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable({destroy:true});
            getMilestone(proj_id,''+user_type+'');
        }
        else{
            $(".loaderr").hide();
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable({destroy:true});
        }
    });
}
var dataMilestone = [];
function getMilestone(proj_id,user_type) {
    var url = "../../milestone/getMilestone";
    $.post(url,{"proj_id":proj_id},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        var editMiles = "";
        var deleteMiles = "";
        if(Status){
            var j = 0;
            dataMilestone = data.data;
            var milestoneData = "";
            for(var i=0; i < dataMilestone.length; i++){
                j = i+1;
                if(user_type == "admin"){
                    editMiles = '<a onclick=editMilestone('+i+',"'+dataMilestone[i].mile_id+'",'+proj_id+',"'+user_type+'")>' +
                        '<i class="fa fa-pencil"></i></a>';
                    deleteMiles = '<a onclick=confirmDeleteMilestone('+dataMilestone[i].mile_id+','+proj_id+',"'+user_type+'")>' +
                        '<i class="fa fa-times"></i></a>';
                }
                milestoneData = milestoneData + '<div class="col-lg-12"><div class="ibox"><div class="ibox-title">' +
                    '<h5>'+dataMilestone[i].mile_name+'</h5><h5 style="margin-left:20%;">Start-Date: ' +
                    ''+dataMilestone[i].mile_start_date+'</h5><h5 style="margin-left:20%;">Deadline: '+dataMilestone[i].mile_end_date+'</h5><div class="ibox-tools"><a class="collapse-link">' +
                    '<i class="fa fa-chevron-up"></i></a>'+editMiles + deleteMiles+'</div></div><div class="ibox-content"><div class="scroll_content">' +
                    '<p>'+dataMilestone[i].mile_description+'</p></div></div></div></div>';
            }
            $(".loaderr").hide();
        }
        else{
            milestoneData = '<div class="col-lg-12"><div class="ibox"><div class="ibox-title">' +
                '<h5>Any milestone not found</h5></div></div></div>';
            $(".loaderr").hide();
        }
        $("#milestoneBox").html(milestoneData);
    });
}
function editMilestone(index,mile_id,proj_id,user_type) {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Update Milestone</h3>' +
        '<p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row">' +
        '<div class="col-sm-12"><div class="form-group"><label>Name</label> <input type="text" ' +
        'placeholder="Miletone Name" class="form-control" id="milestoneName" value="'+dataMilestone[index].mile_name+'"></div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-6">' +
        '<div class="form-group projectDate"><label>Start Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="startDate" type="text" class="form-control" placeholder="start date" ' +
        'value="'+dataMilestone[index].mile_start_date+'"></div></div></div>' +
        '<div class="col-sm-6"><div class="form-group projectDate"><label>Deadline Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="deadlineDate" type="text" class="form-control" placeholder="deadline date"' +
        'value="'+dataMilestone[index].mile_end_date+'"></div></div></div>' +
        '</div>' +
        '<div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<label>Description</label><textarea placeholder="milestone description" id="milestoneDescription"' +
        'class="form-control" rows="2" cols="4">'+dataMilestone[index].mile_description+'</textarea>' +
        '</div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-12"><button class="btn btn-md btn-primary pull-right milestoneBtn" style="margin-top: 25px" ' +
        'onclick=milestoneUpdate('+mile_id+','+proj_id+',"'+user_type+'"); type="button">' +
        '<strong>Update</strong>' +
        '</button>' +
        '</div></div>');
    $("#myModal").modal("show");
    $('.projectDate .input-group.date').datepicker({
        startView: 1,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "d-M-yyyy"
    });
}

function milestoneUpdate(mile_id,proj_id,user_type) {
    var milestoneName = $("#myModal #milestoneName").val();
    var milestoneDescription = $("#myModal #milestoneDescription").val();
    var startDate = $("#myModal #startDate").val();
    var deadlineDate = $("#myModal #deadlineDate").val();
    var proj_emp_id = $("#proj_emp_id").val();
    var url = "../../milestone/updateMilestone";
    if(milestoneName == "" || milestoneDescription == "" || startDate == "" || deadlineDate == ""){
        $("#myModal #err_message").html("please fill required fields");
        $("#myModal #err_message").css("color","red");
        return false;
    }
    $(".loaderr").show();
    $("#myModal .milestoneBtn").prop("disabled",true);
    $.post(url,{"mile_id":mile_id,"proj_emp_id":proj_emp_id,"milestoneName":milestoneName,
        "milestoneDescription":milestoneDescription,
        "startDate":startDate,"deadlineDate":deadlineDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","green");
            setTimeout(function () {
                $(".milestoneBtn").prop("disabled",false);
                $("#myModal").modal("hide");
                $("#err_message").html("");
                getMilestone(proj_id,user_type);
            },3000);
        }
        else{
            $(".loaderr").hide();
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","green");
            $("#myModal .milestoneBtn").prop("disabled",false);
        }
    });
}
function createMilestone(proj_id,user_type) {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Create Milestone</h3><p>Create Milestone today' +
        ' for get your target.</p><p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row">' +
        '<div class="col-sm-12"><div class="form-group"><label>Name</label> <input type="text" ' +
        'placeholder="Miletone Name" class="form-control" id="milestoneName"></div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-6">' +
        '<div class="form-group projectDate"><label>Start Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="startDate" type="text" class="form-control" placeholder="start date"></div></div>' +
        '</div>' +
        '<div class="col-sm-6"><div class="form-group projectDate"><label>Deadline Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="deadlineDate" type="text" class="form-control" placeholder="deadline date"></div></div></div>' +
        '</div>' +
        '<div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<label>Description</label><textarea placeholder="milestone description" id="milestoneDescription"' +
        'class="form-control" rows="2" cols="4"></textarea>' +
        '</div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-12"><button class="btn btn-md btn-primary pull-right milestoneBtn" style="margin-top: 25px" ' +
        'onclick=milestoneCreate('+proj_id+',"'+user_type+'"); type="button">' +
        '<strong>Submit</strong>' +
        '</button>' +
        '</div></div>');
    $("#myModal").modal("show");
    $('.projectDate .input-group.date').datepicker({
        startView: 1,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "d/M/yyyy"
    });
}
function milestoneCreate(proj_id,user_type) {
    var milestoneName = $("#myModal #milestoneName").val();
    var milestoneDescription = $("#myModal #milestoneDescription").val();
    var startDate = $("#myModal #startDate").val();
    var deadlineDate = $("#myModal #deadlineDate").val();
    var proj_emp_id = $("#proj_emp_id").val();
    var currentDate     =  moment().format('DD-MM-YYYY hh:mm:ss');
    var url = "../../milestone/addMilestone";

    if(milestoneName == "" || milestoneDescription == "" || startDate == "" || deadlineDate == ""){
        $("#myModal #err_message").html("please fill required fields");
        $("#myModal #err_message").css("color","red");
        return false;
    }
    $(".loaderr").show();
    $(".milestoneBtn").prop("disabled",true);
    $.post(url,{"proj_id":proj_id,"proj_emp_id":proj_emp_id,"milestoneName":milestoneName,
        "milestoneDescription":milestoneDescription,"startDate":startDate,"deadlineDate":deadlineDate,
        "currentDate":currentDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","green");
            setTimeout(function () {
                $(".milestoneBtn").prop("disabled",false);
                $("#myModal").modal("hide");
                getMilestone(proj_id,user_type);
            },3000);
        }
        else{
            $(".loaderr").hide();
            $(".milestoneBtn").prop("disabled",false);
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","red");
        }
        getMilestone(proj_id,user_type);
    });

}

function confirmDeleteMilestone(mile_id,proj_id,user_type) {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Delete Project</h3>' +
        '<p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<label>Are you sure want to delete this milestone ?</label></div></div></div>' +
        '<hr><div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<button data-dismiss="modal" class="btn btn-md btn-primary pull-right" style="margin: 0 5px" ' +
        ' type="button"><strong>Cancel</strong></button><button class="btn btn-md btn-danger pull-right mileDltBtn"' +
        'style="margin: 0 5px"  type="button" onclick=milestoneDelete("'+mile_id+'","'+proj_id+'","'+user_type+'");><strong>Submit</strong></button></div></div></div>');
    $("#myModal").modal("show");
}
function milestoneDelete(mile_id,proj_id,user_type) {
    var url = "../../milestone/deleteMilestone";
    $(".loaderr").show();
    $("#myModal .mileDltBtn").prop("disabled",true);
    $.post(url,{"mile_id":mile_id},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status){
            $(".loaderr").hide();
            $("#myModal #err_message").html(Message);
            $("#myModal #err_message").css("color","green");
            setTimeout(function () {
                $("#myModal .mileDltBtn").prop("disabled",false);
                $("#myModal #err_message").html("");
                $("#myModal").modal("hide");
            },3000);
            getMilestone(proj_id,user_type);
        }
        else{
            $("#myModal #err_message").html(Message);
            $("#myModal .mileDltBtn").prop("disabled",false);
            $(".loaderr").hide();
        }
    });
}

function projectComments(proj_id) {
    window.location = "../../comments/view/"+proj_id;
}