/**
 * Created by SachTech on 28-12-2017.
 */
var statusData = [];

window.onload = function () {
    getAllStatus();
};
function getAllStatus() {
    var url = 'status/getAllStatus';
    var th_ = '<thead><tr><th>#</th><th>Name</th>Project<th>Created AT</th><th>Updated AT</th>' +
              '<th style="text-align: center">Action</th></tr></thead>';
    var td_='<tbody>';
    $.get(url,function (data) {
        console.log('data --- '+JSON.stringify(data));
        var status = data.Status;
        var message = data.Message;
        if(status) {
            /*for delete <button class="btn btn-danger btn-sm" onclick=deleteStatus("'+i+'");>' +
                    '<i class="fa fa-trash-o"></i></button>*/
            statusData = data.data;
            for(var i=0;i<statusData.length;i++) {
                td_ = td_+'<tr><td>'+(i+1)+'</td><td class="capital-first-letter">'+statusData[i].status_title+'</td>' +
                    '<td>'+statusData[i].status_created_at+'</td><td>'+statusData[i].status_updated_at+'</td>' +
                    '<td style="text-align: center"><button class="btn btn-primary btn-sm" onclick="editStatus('+i+')">' +
                    '<i class="fa fa-pencil"></i></button>&nbsp;&nbsp;</td></tr>';
            }
            $("#typeData").html(th_+td_+'</tbody>');
        }
        $("#typeData").html(th_+td_+'</tbody>');
        $("#typeData").dataTable({destroy:true});
    });
}

function onCreateProjStatus() {
    var proj_status = $("#proj_status").val();
    var created_at = moment().format("DD-MM-YYYY");
    if(proj_status === '') {
        $("#error").html("Please enter project status..");
        $("#error").css("color","red");
        return false;
    }
    var url = 'status/createProjectStatus';
    var type_data = {"proj_status":proj_status,"created_at":created_at};
    $.post(url,type_data,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#myModal").modal("hide");
        getAllStatus();

    });
}

function editStatus(index) {
    var status_id = statusData[index].status_id;
    var status_title = statusData[index].status_title;
    var projectOption = "";
    $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Update Status" +
        "</label></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<div class='col-md-6 no-padding' style='padding-left:1% !important; '>" +
        "<label>Project Status</label><input type='text' id='proj_status' class='form-control' placeholder='project status'" +
        "value='" + status_title + "'>" +
        "</div>" +
        "<div class='col-md-12 no-padding' style='margin-top: 2%'><button class='btn btn-primary btn-sm' " +
        "onclick=onUpdateStatus('" + status_id + "');><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm' data-dismiss='modal'  style='margin-left: 1%'>" +
        "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
    $("#myModal").modal("show");
}

function onUpdateStatus(id) {
    var proj_status = $("#proj_status").val();
    if( proj_status === '') {
        $("#error").html("Please enter project status..");
        $("#error").css("color","red");
        return false;
    }
    var url = 'status/status_update';
    $.post(url,{'statusId':id,'proj_status':proj_status},function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#myModal").modal("hide");
        getAllStatus();

    });

}
function deleteStatus(index) {
    var type_id = statusData[index].status_id;
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Delete</label></div>' +
        ' <div class="col-md-12" style="margin-top: 2%;"><p>Would you like to remove status (y/n) ?</p></div>' +
        '<div class="col-md-12" style="margin-top: 2%;padding-top: 2%;border-top:1px #eee solid"><button ' +
        'class="btn btn-danger btn-sm" onclick="onDeleteStatus('+type_id+');"><i class="fa fa-floppy-o"></i> Remove</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" data-dismiss="modal" ><i class="fa fa-trash-o"></i> ' +
        'Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}

function createStatus() {
    var projectOption = "";
    var url = "dashboard/getAllProjects";
    $.post(url,function (data) {
        var Status = data.Status;
        var Message = data.Message;
        if (Status == "Success") {
            var projectTypeData = data.AllProject;
            for (var j = 0; j < projectTypeData.length; j++) {
                projectOption = projectOption + "<option value='" + projectTypeData[j].proj_id + "'>" + projectTypeData[j].proj_name + "</option>";
            }
        }
        else {
            console.log(Status + " " + Message);
        }
        $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Create</label>" +
            "</div><div class='col-md-12 no-padding' style='margin-top: 2%'><div class='col-md-6 no-padding' style='padding-left:1% !important; '>" +
            "<label>Project Status</label><input type='text' placeholder='project status' id='proj_status' class='form-control'></div>" +
            "</div><div class='col-md-12 no-padding' style='margin-top: 2%'><button class='btn btn-primary btn-sm' onclick='onCreateProjStatus();'><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm' data-dismiss='modal' style='margin-left: 1%'>" +
            "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
        $("#myModal").modal("show");
    });
}
function onDeleteStatus(status_id) {
    var url = 'status/removeStatus';
    $.post(url,{"status_id":status_id},function (data) {

        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllStatus();
        }
        else{
            $("#error").html(message);
            $("#error").css("color","red");
        }

    });
}