/**
 * Created by SachTech on 28-12-2017.
 */

var types = [];

window.onload = function () {
  getAllTypes();
};

function getAllTypes() {
    var url = 'projecttypee/types';
    var th_ = '<thead><tr><th>#</th><th>Name</th><th>Created AT</th><th>Updated AT</th><th style="text-align: center">Action</th></tr></thead>';
    var td_='<tbody>';
    $.get(url,function (data) {
       //console.log('data --- '+JSON.stringify(data));
       var status = data.Status;
       var message = data.Message;
       if(status) {
          types = data.data;
          for(var i=0;i<types.length;i++) {
              td_ = td_+'<tr><td>'+(i+1)+'</td><td>'+types[i].type_name+'</td><td>'+types[i].type_created_at+'</td>'+
                  '<td>'+types[i].type_updated_at+'</td><td style="text-align: center"><button class="btn btn-primary btn-sm" onclick="editType('+i+')"><i class="fa fa-pencil"></i></button>' +
                  '&nbsp;&nbsp;<button class="btn btn-danger btn-sm" onclick=deleteType("'+i+'");><i class="fa fa-trash-o"></i></button></td></tr>';
          }
           $("#typeData").html(th_+td_+'</tbody>');
       }
        $("#typeData").html(th_+td_+'</tbody>');
        $("#typeData").dataTable({destroy:true});
    });
}

function createType() {
    $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Create</label></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<div class='col-md-6 no-padding'><label>Title</label><input type='text' placeholder='Enter project type' class='form-control' id='type_name'/></div><div class='col-md-6 no-padding' style='padding-left:1% !important; '>" +
        "<label>Status</label><select id='type_status' class='form-control'><option value='A'>Active</option><option value='D'>Disabled</option></select></div>" +
        "</div><div class='col-md-12 no-padding' style='margin-top: 1%'><label>Description</label><textarea class='form-control' style='resize: none' id='type_desc'></textarea></div>"+
        "<div class='col-md-12 no-padding' style='margin-top: 2%'><button class='btn btn-primary btn-sm' onclick='onCreateType();'><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm' data-dismiss='modal' style='margin-left: 1%'>" +
        "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
    $("#myModal").modal("show");
}

function onCreateType() {
    var type_name = $("#type_name").val();
    var type_desc = $("#type_desc").val();
    var type_status = $("#type_status").val();
    var created_at = moment().format("DD-MM-YYYY");
    if(type_name === '' || type_desc === '') {
        $("#error").html("Please enter all fields..");
        $("#error").css("color","red");
        return false;
    }
    var url = 'projecttypee/type_create';
    var type_data = {type_name:type_name,type_status:type_status,type_desc:type_desc,type_created_at:created_at};
    $.post(url,type_data,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#myModal").modal("hide");
        getAllTypes();

    });
}

function editType(index) {
    var type_id = types[index].type_id;
    var type_name = types[index].type_name;
    var type_status = types[index].type_status;
    var type_desc = types[index].type_desc;

    $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Create</label></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<div class='col-md-6 no-padding'><label>Title</label><input type='text' placeholder='Enter project type' class='form-control' id='type_name'  value='"+type_name+"'></div><div class='col-md-6 no-padding' style='padding-left:1% !important; '>" +
        "<label>Status</label><select id='type_status' class='form-control'><option value='A'>Active</option><option value='D'>Disabled</option></select></div>" +
        "</div><div class='col-md-12 no-padding' style='margin-top: 1%'><label>Description</label><textarea class='form-control' style='resize: none' id='type_desc'>"+type_desc+"</textarea></div>"+
        "<div class='col-md-12 no-padding' style='margin-top: 2%'><button class='btn btn-primary btn-sm' onclick=onUpdateType('"+type_id+"');><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm' data-dismiss='modal'  style='margin-left: 1%'>" +
        "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
    $("#myModal").modal("show");
    $("#type_status").val(type_status);
}

function onUpdateType(id) {
    var type_name = $("#type_name").val();
    var type_desc = $("#type_desc").val();
    var type_status = $("#type_status").val();
    if(type_name === '' || type_desc === '') {
        $("#error").html("Please enter all fields..");
        $("#error").css("color","red");
        return false;
    }
    var url = 'projecttypee/type_update';

    $.post(url,{'type_id':id,'type_name':type_name,'type_status':type_status,'type_desc':type_desc},function (data) {
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#error").html(message);
            $("#error").css("color","red");
            return false;
        }
        $("#myModal").modal("hide");
        getAllTypes();

    });
}
function deleteType(index) {
    var type_id = types[index].type_id;
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Delete</label></div> <div class="col-md-12" style="margin-top: 2%;">' +
        '<p>Would you like to remove type (y/n) ?</p></div><div class="col-md-12" style="margin-top: 2%;padding-top: 2%;border-top:1px #eee solid"><button class="btn btn-danger btn-sm" onclick="onDeleteType('+type_id+');"><i class="fa fa-floppy-o"></i> Remove</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" data-dismiss="modal" onclick=onDismissDialog("myModal"); ><i class="fa fa-trash-o"></i> Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}

function onDeleteType(type_id) {
    var url = 'projecttypee/type_remove';
    $.post(url,{type_id:type_id},function (data) {

        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllTypes();
        }
        else{
            $("#error").html(message);
            $("#error").css("color","red");
        }

    });
}