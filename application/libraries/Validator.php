<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 26-Sep-16
 * Time: 11:40 AM
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Validator {

    function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->config('validate_message');
    }

    /*
     * check for valid email-id
     * */
    public function isValidEmail($email) {
        $reg = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

        if (preg_match($reg, $email)) {
            return true;
        }
        return false;

    }

    /*
     * check  for valid mobile number..
     *
     * */

    public function validate_mobile_number($number){
//        echo preg_match("/^[0-9]{10}*$/",$number)."<br/>";
//		echo strlen($number);

        if(!preg_match('/^0\d{10}$/',$number) && strlen($number)!= 10)
        {
            return false;
        }
        return true;
    }

    /*
     * in this function params will be validated with post or get params
     * */

    public function valid_params($params,$requiredfields) {
        $error = "";
        $keys = array_keys($params);
        if(count($params) == count($requiredfields))
        {
            for($i=0;$i<count($requiredfields);$i++)
            {
                $feild = $requiredfields[$i];
                if(in_array($feild, $keys))
                {
                    if($params[$feild]=="")
                    {
                        $error = $error.$feild.",";
                    }
                }
                else
                {
                    $error = $error.$feild.",";
                }
            }
            $error = trim($error);
        }
        else
        {
            for($i=0;$i<count($requiredfields);$i++)
            {
                $feild = $requiredfields[$i];
                if(in_array($feild, $keys))
                {
                    if($params[$feild]=="")
                    {
                        $error = $error.$feild.",";
                    }
                }
                else
                {
                    $error = $error.$feild.",";
                }
            }
            $error = trim($error);
        }
        if($error != "")
        {
            $error = rtrim($error,",");
            $response[$this->CI->config->item('status')]= false;
            $response[$this->CI->config->item('message')]= 'The following fields are required: '.$error;
            return $response;
        }
        $response[$this->CI->config->item('status')] = true;
        $response[$this->CI->config->item('message')]= '';

        return $response;
    }

    public function apiResponse($response) {
        header("Content-Type: application/json");
        echo json_encode($response);

    }
}
?>