<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 28-12-2017
 * Time: 12:20
 */
class Dbresults
{
  function __construct()
  {
      $this->CI =& get_instance();
      $this->CI->load->database();
  }

    public function get_data($table_name,$params,$where,$order_by = null) {
        $this->CI -> db -> select($params);
        $this->CI -> db -> from($table_name);
        if($order_by){
            foreach ($order_by as $key => $value){}
            $this->CI -> db -> order_by($key,$value);
        }
        $this->CI ->db-> where($where);
        $query = $this->CI -> db -> get();

      if($query -> num_rows() >0)
      {
          return $query->result();
      }
      return false;
  }

  public function post_data($table_name,$data) {
      $this->CI -> db->insert($table_name, $data);
      $proj_id = $this->CI -> db->insert_id();
      if($proj_id>0) {
          return true;
      }
      return false;
  }

  public function update_data($table_name,$data,$where) {
      $this->CI -> db->where($where);
      $this->CI -> db->update($table_name, $data);
        $afftectedRows = $this->CI -> db->affected_rows();
        if($afftectedRows>0) {
            return true;
        }
     return false;
  }

  public function delete_data($table_name,$where) {

      $this->CI -> db->where($where);
      $this->CI -> db->delete($table_name);
      $afftectedRows = $this->CI ->db->affected_rows();

      if($afftectedRows>0) {
          return true;
      }
      return false;
  }

    public function do_upload($config,$name){
        /*$config = array(
            'upload_path' => "./uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "768",
            'max_width' => "1024"
        );*/
        $this->CI -> db->load->library('upload', $config);
        if($this->CI -> db->upload->do_upload($name))
        {
            return $this->CI ->upload->data();
        }
        else
        {
            $error = array('error' => $this->CI ->upload->display_errors());
            return $error;
        }
    }
    public function send_mail($to,$bodyData,$subject){
        $this->CI->load->library('email');
        $config = array(
            'protocol'  => 'smtp',
            'smtp_host' => 'md-in-68.webhostbox.net',
            'smtp_port' => 465,
            'smtp_user' => 'noreply@stsmentor.com',
            'smtp_pass' => 'noreply@sts',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        );
        $this->CI->email->initialize($config);
        $this->CI->email->set_mailtype("html");
        $this->CI->email->set_newline("\r\n");

        $this->CI->email->to($to);
        $this->CI->email->from('noreply@stsmentor.com',$subject);
        $this->CI->email->subject('');
        $this->CI->email->message($bodyData);
        return $this->CI->email->send();
    }


}