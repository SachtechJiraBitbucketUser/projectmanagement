<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PROJECT || MANAGEMENT</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.png">
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
    <input type="hidden" value="<?php echo base_url();?>" id="baseUrl";
</head>
<style>
 /*   body{
        background-image: url("<?php echo base_url();?>/assets/img/companypro2.png");
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
        width: 100%;
    }*/
</style>
<body class="gray-bg">

<div class="loaderr" id="loadd">
    <img src="<?php echo base_url();?>assets/img/762.gif" class="loaderimg">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">PM</h1>
        </div>
        <h3>Welcome to Project Management</h3>
        <p>Perfectly Manage Your Project.<br>
            Enter your email address , we will send you otp at this email address for password change.
        </p>
        <form class="m-t" role="form">
            <div class="form-group">
                <input type="email" class="form-control" placeholder="Enter email-id" id="emp_email">
            </div>
            <div class="form-group forgotPasswordBox">
                <input type="text" class="form-control" placeholder="enter otp" id="otp">
            </div>
            <div class="form-group forgotPasswordBox">
                <input type="password" class="form-control " placeholder="new password" id="newPass">
            </div>
            <div class="form-group forgotPasswordBox">
                <input type="password" class="form-control " placeholder="confirm password" id="confirmPass">
            </div>
            <div class="form-group">
                <a href="<?php echo base_url();?>user/" class="text-left">
                    <p class="m-t text-color-underline">Login</p>
                </a>
            </div>
            <button type="button" class="btn btn-primary full-width m-b sendOtpBtn" onclick="sendOtp();">Submit</button>
            <button type="button" class="btn btn-primary full-width m-b otpValidBtn"
                    style="display: none" onclick="otpValidUser();">Submit</button>
        </form>
        <p class="m-t"> <small><!--Inspinia we app framework base on Bootstrap 3 &copy; 2014--></small> </p>
    </div>
</div>
<!-- for message modal start here-->
<div id="showMessage" class="modal fade" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" id="main_row">
                    <div class="col-sm-6 b-r"><h3 class="m-t-none m-b"></h3>
                        <p></p>
                        <form role="form">
                            <div class="form-group"><label>Email</label> <input type="email" placeholder="Enter email" class="form-control"></div>
                            <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div>
                            <div>
                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Log in</strong></button>
                                <label> <input type="checkbox" class="i-checks"> Remember me </label>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6"><h4>Not a member?</h4>
                        <p>You can create an account:</p>
                        <p class="text-center">
                            <a href="#"><i class="fa fa-sign-in big-icon"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Mainly scripts -->
<script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/user.js"></script>
</body>
</html>
