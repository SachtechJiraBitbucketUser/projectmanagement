<!--body content start here-->

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Dashboard/Project</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <h5 class="headerProjHeading">Total Projects</h5>
                </div>
                <!--for get all project send 1 always-->
                <a href="<?php echo base_url(); ?>project/1">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_total_projects"></h1>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <h5 class="headerProjHeading">Projects On Hold</h5>
                </div>
                <a href="<?php echo base_url(); ?>project/8">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_hold_projects"></h1>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <h5 class="headerProjHeading">Projects New</span></h5>
                </div>
                <a href="<?php echo base_url(); ?>project/5">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_projects_new"></h1>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <h5 class="headerProjHeading">Projects Running</h5>
                </div>
                <a href="<?php echo base_url(); ?>project/10">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_projects_running"> </h1>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <h5 class="headerProjHeading">Projects Live</h5>
                </div>
                <a href="<?php echo base_url(); ?>project/6">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_projects_live"></h1>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <h5 class="headerProjHeading">Projects Closed</h5>
                </div>
                <a href="<?php echo base_url(); ?>project/7">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_projects_closed"></h1>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-2" style="width: 14.28%">
            <div class="ibox float-e-margins heading-titless">
                <div class="ibox-title ">
                    <span class="label label-primary pull-right"></span>
                    <h5 class="headerProjHeading">Projects Archived</h5>
                </div>
                <a href="<?php echo base_url(); ?>project/9">
                    <div class="ibox-content">
                        <h1 class="no-margins" id="top_projects_archived"></h1>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total Projects </h5>
                    <div class="ibox-tools" id="createProjectBox">

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive" >
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="projectData">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>
<script src="<?php echo base_url();?>assets/js/projectProcess.js"></script>
<script>
    getProjectCount();
    getAllProjects(<?php echo $session['proj_emp_id']; ?>);
</script>
