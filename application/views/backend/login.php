<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PROJECT || MANAGEMENT</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
</head>
<body class="gray-bg">
<div class="loaderr" id="loadd">
    <img src="<?php echo base_url();?>assets/img/762.gif" class="loaderimg">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">PM</h1>
            </div>
            <h3>Welcome to Project Management</h3>
            <p>Perfectly Manage Your Project.
                <!--Continually expanded and constantly improved Inspinia Admin Them (IN+)-->
            </p>
            <p>Login in. To see it in action.</p>
            <form class="m-t" role="form">
                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Enter email-id" id="email_id">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Enter password" id="password">
                </div>
                <div class="form-group">
                    <a href="<?php echo base_url();?>user/forgot">
                        <p class="m-t text-left text-color-underline"> <small>Forgot Password</small> </p>
                    </a>
                </div>
                <button type="button" class="btn btn-primary block full-width m-b" onclick="onLoginPress();">Login</button>
            </form>
            <p class="m-t"> <small><!--Inspinia we app framework base on Bootstrap 3 &copy; 2014--></small> </p>
        </div>
    </div>
    <!-- for message modal start here-->
    <div id="showMessage" class="modal fade" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row" id="main_row">
                        <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Sign in</h3>
                            <p>Sign in today for more expirience.</p>
                            <form role="form">
                                <div class="form-group"><label>Email</label> <input type="email" placeholder="Enter email" class="form-control"></div>
                                <div class="form-group"><label>Password</label> <input type="password" placeholder="Password" class="form-control"></div>
                                <div>
                                    <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Log in</strong></button>
                                    <label> <input type="checkbox" class="i-checks"> Remember me </label>
                                </div>
                            </form>
                        </div>
                        <div class="col-sm-6"><h4>Not a member?</h4>
                            <p>You can create an account:</p>
                            <p class="text-center">
                                <a href="#"><i class="fa fa-sign-in big-icon"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="<?php echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/user.js"></script>

</body>


<!-- Mirrored from webapplayers.com/inspinia_admin-v2.login.phphtml by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 26 Dec 2017 08:15:08 GMT -->
</html>

