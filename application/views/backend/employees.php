<!--body content end here-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Employee</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total Employees </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-primary" onclick="createEmployee();">Create Employee</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <!--all project data start here -->
                    <div class="table-responsive" >
                        <table class="table table-striped table-bordered table-hover dataTables-example" id="userData">
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include ('footer.php');?>
<script src="<?php echo base_url();?>assets/js/user.js"></script>
<script>
    getAllUsers();
    getFieldsData(<?php echo $session['proj_emp_id']; ?>);
</script>
<!--body content end here-->
