<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/3/2018
 * Time: 5:52 PM
 */
echo "<input type='hidden' id='proj_emp_id' value='".$session['proj_emp_id']."'>";
?>

<!--body content start here-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Create Project</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title text-center">
                    <h5 id="top_title"> </h5>
                </div>
                <div class="ibox-content">
                    <!--all project data start here -->
                    <div id="createEmployeeBox">
                        <div class="row" >
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Project Id</label>
                                    <input type="text" placeholder="Project Id" class="form-control" readonly id="projectId" value="1">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Development Area</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <select class="form-control" id="projectType">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Project Name</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <input type="text" placeholder="Project Name" class="form-control capital-first-letter"  id="projectName" >
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Project Status</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <select class="form-control" id="projectStatus">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Project Manager</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <div class="input-group date">
                                        <span class="input-group-addon" onclick="showEmployeeList('Manager')">
                                            <i class="fa fa-smile-o"></i>
                                        </span>
                                        <select class="form-control" id="projectManager" disabled style="">
                                            <option value="">Project Manager</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>Project Developer</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon" onclick="showEmployeeList('Developer')">
                                            <i class="fa fa-smile-o"></i>
                                        </span>
                                        <select id="projectTeamDeveloper" data-placeholder="Developer..."
                                            disabled class="js-example-basic-multiple" multiple ></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group projectDate">
                                    <label>Project Status Comments</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <input type="text" placeholder="Project Status Comments" class="form-control" id="projectComment" >
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group projectDate">
                                    <label>Project Start Date</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                        <input id="startDate" type="text" class="form-control datepickerCreate" placeholder="Start Date">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" >
                            <div class="col-sm-3">
                                <div class="form-group projectDate">
                                    <label>Expected Delivery Date</label>
                                    <i class="fa fa-star reqdFields"></i>
                                    <div class="input-group date">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                        <input id="delieveryDate" type="text" class="form-control datepickerCreate" placeholder="Expected Delivery Date">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group projectDate">
                                    <label>Actual Completion Date</label>
                                    <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                        <input id="closingDate" type="text" class="form-control datepickerCreate" placeholder="Actual Completion Date">
                                    </div>
                                </div>
                            </div>
                            <?php
                                $userType = "";
                                if(isset($_SESSION['proj_emp_type'])){
                                    $userType = $_SESSION['proj_emp_type'];
                                    if($userType == "admin"){
                                        ?>

                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label>Project Type</label>
                                                <i class="fa fa-star reqdFields"></i>
                                                <select class="form-control" id="amountType" onchange=projTypeChange(this)>
                                                    <option value="">Select</option>
                                                    <option value="fixedPrice">Fixed Price</option>
                                                    <option value="timeMaterial">Time & Material</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-3 fixedPriceBox" >
                                            <div class="form-group">
                                                <label>Total Project Cost</label>
                                                <input type="text" class="form-control" id="totalProjCost" placeholder="Total Project Cost">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 timeMaterialBox">
                                            <div class="form-group">
                                                <label>Number of hours worked</label>
                                                <input type="text" class="form-control" placeholder="Number of hours worked" id="totalProjMember">
                                            </div>
                                        </div>
                        </div>
                        <div class="row">
                                        <div class="col-sm-3 timeMaterialBox">
                                            <div class="form-group">
                                                <label> Hourly Rate</label>
                                                <input type="text" class="form-control" placeholder="Hourly Rate"
                                                       id="totalProjHours">
                                            </div>
                                        </div>
                                        <div class="col-sm-3 timeMaterialBox">
                                            <div class="form-group">
                                                <label>Total Project Cost</label>
                                                <input type="text" class="form-control" placeholder="Total Project Cost"
                                                       id="timeMaterialProjectCost">
                                            </div>
                                        </div>
                                        <div id="milestoneBoxes">
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label>Project Milestone 1</label>
                                                    <textarea class="form-control projectMilestone" id="projectMilestone"
                                                              name="DynamicTextBox" placeholder="Milestone"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <button id="milestoneAdd" type="button" class="btn btn-default">
                                                    Milestone +
                                                </button>
                                                <input id="btnGet" type="button" value="Get Values" hidden />
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                            <?php
                                }
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="<?php echo base_url();?>">
                                    <button class="btn  btn-default pull-right" style="margin-left: 5px;margin-right: 5px"
                                            type="button">
                                        Cancel
                                    </button>
                                </a>
                                <button class="btn btn-md btn-primary pull-right" style=""
                                        onclick="creatProject('<?php echo $_SESSION['proj_emp_type']; ?>')" type="button">
                                    <strong>Submit</strong>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="employeeManagerListBox" class="employeeListBox">
                        <div class="row">
                        </div>
                    </div>
                    <div id="employeeDeveloperListBox" class="employeeListBox">
                        <div class="row">
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<!--body content end  here-->
<?php include("footer.php"); ?>
<script src="<?php echo base_url(); ?>assets/js/createProject.js"></script>
<script>
    $("#proj_emp_id").val(<?php echo $session['proj_emp_id']; ?>);
    getFieldData(<?php echo $session['proj_emp_id']; ?>,"createProject");
    $('.datepickerCreate').datepicker({
        startView: 1,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'd/M/yyyy'
    });

</script>

