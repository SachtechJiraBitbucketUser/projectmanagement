<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/9/2018
 * Time: 11:25 AM
 */
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>My Profile</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile Detail</h5>
                </div>
                <div>
                    <div class="ibox-content no-padding border-left-right " style="background: white">
                        <div class="row" style="background: white!important;margin: 0;">
                            <div class="col-lg-4" style="position: relative;background: white">
                                <img alt="image" class="img-responsive oldUserImg" id="user_image"
                                     style="max-width: 100%" src="<?php echo base_url() ?>assets/img/userImg/dummy_user.png">
                                <input type="file" class="user_image_file" id="user_image_file" onchange=readURL(this,'oldUserImg'); />
                            </div>
                            <div class="col-lg-8 mt-10" style="background: white;">
                                <h5 class="capital-first-letter">Name : <span class="user_name capital-first-letter bold-text"></span></h5>
                                <h5>Email-Id : <span class="user_email bold-text"></span></h5>
                                <h5 >Designation : <span class="user_designation bold-text"></span></h5>
                                <h5 >Mobile : <span class="user_mobile bold-text"></span></h5>
                            </div>
                        </div>
                        <div>
                            <button type="button" class="btn btn-primary mt-10 ml-10 userImgBtn" onclick="changeUserImage('<?php echo $session['proj_emp_id']; ?>')">
                                Change Profile
                            </button>
                        </div>
                    </div>
                    <div class="ibox-content profile-content">
                        <h5 style="background: white">Message :
                            <p class="user_message bold-text">

                            </p>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Change Info <span id="err_message"></span></h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Name </label>
                            <input type="text" class="form-control" id="user_name_field" placeholder="name" />
                        </div>
                        <div class="col-lg-6">
                            <label>Email  </label>
                            <input type="text" class="form-control" id="user_email_field" placeholder="email Address" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <label>Designation  </label>
                            <select class="form-control" id="user_designation_field">
                                <option value="">Select</option>
                                <?php
                                 for($k = 0; $k < count($desig_data); $k++){
                                     echo '<option value="'.$desig_data[$k]->desig_id.'">'.$desig_data[$k]->desig_title.'</option>';
                                 }
                                ?>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label>Mobile </label>
                            <input type="text" class="form-control only-numbers" id="user_mobile_field" placeholder="mobile" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <label>Message  </label>
                            <textarea class="form-control" id="user_message_field" placeholder="message" ></textarea>
                        </div>
                        <div class="col-lg-12 mt-10">
                            <button type="button" class="btn btn-primary pull-right update_info" onclick="updateUserInfo('<?php echo $session['proj_emp_id']; ?>')">
                                Update Info
                            </button>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>
<script src="<?php echo base_url();?>assets/js/user.js"></script>

 <script>
     getPartUser(<?php echo $session['proj_emp_id']; ?>);
 </script>

