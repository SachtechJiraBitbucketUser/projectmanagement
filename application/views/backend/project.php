<!--body content start here-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>All Project</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total Project </h5>
                    <div class="ibox-tools" id="createProjectBox">
                        <!--<a  class="btn btn-primary"  href="<?php /*base_url(); */?>createProject/view/">Create Project</a>-->
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive" >
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="projectData">

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>
<script src="<?php echo base_url();?>assets/js/projectProcess.js"></script>
<script>
    getStatusProjects(<?php echo $session['proj_emp_id']; ?>,<?php echo $status_value; ?>);
</script>
