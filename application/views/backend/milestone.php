<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/29/2017
 * Time: 12:12 PM
 */
if(isset($session['proj_emp_id'])) {
    echo "<input type='hidden' id='proj_emp_id' value='".$session['proj_emp_id']."'>";
}
?>
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Milestone</h2>
    </div>
    <div class="col-lg-2">
        <div class="ibox-tools" id="createMilestone">

        </div>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive" >
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="projectData">

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row m-t-lg" id="milestoneBox">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Milestone 1</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="scroll_content">
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>

<?php include("footer.php"); ?>
<script src="<?php echo base_url();?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>assets/js/milestone.js"></script>
<script>
    getProjects(<?php echo $proj_id;?>,<?php echo $session['proj_emp_id']; ?>);
    $(document).ready(function () {
        // Add slimscroll to element
        $('.scroll_content').slimscroll({
            height: '150px'
        })

    });
</script>
