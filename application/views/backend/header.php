<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PROJECT MANAGEMENT</title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/img/favicon.png">
    <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/select2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
    <?php echo "<input type='hidden' id='baseUrl' value='".base_url()."'/>"; ?>
</head>
<body>

<div class="loaderr" id="loadd">
    <img src="<?php echo base_url();?>assets/img/762.gif" class="loaderimg">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle userHeaderProfile" src="<?php echo base_url(); ?>assets/img/dummy_user.png" />
                             </span>
                        <?php
                        if(isset($session['proj_emp_id'])) {
                            ?>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                             <span class="clear">
                                 <span class="block m-t-xs">
                                     <strong class="font-bold" id="userNameTitle">

                                     </strong>
                                </span>
                                <!-- <span class="text-muted text-xs block"><?php /*echo $session['proj_emp_role'];*/?>
                                     <b class="caret"></b>
                                 </span>-->
                             </span>
                            </a>
                            <?php
                        }
                        else{
                            ?>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                  <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Default user</strong>
                                  </span> <span class="text-muted text-xs block">Developer <b class="caret"></b></span> </span>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="logo-element">
                        PM
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links">
                    <li>
                        <h2 class="m-r-sm text-muted welcome-message">Welcome to STS Project Management</h2>
                    </li>
                    <li class="pull-right">
                        <a href="<?php echo base_url();?>logout/signout" >
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <!--body content start here-->
        <!--<div class="wrapper wrapper-content"></div>-->

<script>
    function jsStringfirst(string)
    {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    /*get menu according to privlege call from footer*/
    function getMenuUser(userId){
        var pageName = $("#page_name").val();
        var base_url = $("#baseUrl").val();
        var url = "<?php echo base_url();?>dashboard/getAllMenu";
        $.post(url,{"emp_id":userId},function(data){
            var Status = data.Status;
            var Message =  data.Message;
            if(Status){
                var menu_list = "";
                var menuData = data.menuData;
                var empData = data.data;
                if(empData[0].emp_img != ""){
                    $(".userHeaderProfile").attr("src",base_url+"assets/img/userImg/"+empData[0].emp_img);
                }
                if(empData[0].emp_name != ""){
                    $("#userNameTitle").html(empData[0].emp_name);
                }
                var MenuName ="";
                for(var j = 0; j < menuData.length; j++){
                    MenuName = menuData[j].menu_name;
                    MenuName = MenuName.toLowerCase();
                    menu_list = menu_list + '<li id="'+MenuName+'"><a href="'+base_url+menuData[j].menu_page+'">' +
                        '<i class="fa fa-th-large"></i><span class="nav-label">'+menuData[j].menu_name+'</span></a></li>';
                }
                $("#side-menu").append(menu_list+'<li id="myprofile"><a href="'+base_url+'User/profileData"><i class="fa fa-th-large"></i>' +
                    '<span class="nav-label">My Profile</span></a></li>');
                if(pageName == "myprofile"){
                    $("#myprofile").addClass("active");
                }
                else{
                    $("#"+pageName).addClass("active");
                }
            }
            else{
                alert(Status +" "+ Message);
            }
        });
    }
</script>
