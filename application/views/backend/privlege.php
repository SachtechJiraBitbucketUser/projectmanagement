<style>
    .no-gutter{
        margin: 0px;
        padding: 0px;
    }
    .privlegeHeading{
        font-weight: bold;
    }
    .userPrivlegeBox{
        display: none;
    }
</style>
<!--body content end here-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Privlege</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins privlegeData">
                <div class="ibox-title">
                    <h5>Privlege</h5>
                </div>
                <div class="ibox-content ">
                    <!--all project data start here -->
                    <div class="table-responsive" >
                        <table class="table table-striped" id="privlegeData">


                        </table>
                    </div>
                </div>
            </div>

            <div class="ibox float-e-margins userPrivlegeBox">
                <div class="ibox-content ">
                    <div class="table-responsive" id="userPrivlegeBox">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php';?>
<script src="<?php echo base_url();?>assets/js/privlegeProcess.js"></script>
<!--body content end here-->