<style>
    .no-gutter{
        margin: 0px;
        padding: 0px;
    }
</style>
<!--body content end here-->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Project Type</h2>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Total Project Type </h5>
                    <div class="ibox-tools">
                        <a class="btn btn-primary" onclick="createType()">Create Type</a>
                    </div>
                </div>
                <div class="ibox-content">

                    <!--all project data start here -->
                    <div class="table-responsive">
                        <table class="table table-striped" id="typeData">
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php'); ?>
<script src="<?php echo base_url();?>assets/js/typeProcess.js"></script>

<!--body content end here-->
