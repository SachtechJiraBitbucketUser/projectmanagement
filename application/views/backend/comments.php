<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/28/2017
 * Time: 12:12 PM
 */
    if(isset($session['proj_emp_id'])) {
        echo "<input type='hidden' id='proj_emp_id' value='".$session['proj_emp_id']."'>";
    }
?>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Comments</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="table-responsive" >
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="projectData">

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row m-t-lg">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div>
                       <div class="chat-activity-list" id="commentBox">
                            <!--<div class="chat-element">
                               <a href="#" class="pull-left">
                                    <img alt="image" class="img-circle" src="<?php /*echo base_url();*/?>assets/img/a2.jpg">
                                </a>
                                <div class="media-body ">
                                    <small class="pull-right text-navy">1m ago</small>
                                    <strong>Mike Smith</strong>
                                    <p class="m-b-xs">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                                    </p>
                                    <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                </div>
                            </div>
                            <div class="chat-element right">
                                <a href="#" class="pull-right">
                                    <img alt="image" class="img-circle" src="<?php /*echo base_url();*/?>assets/img/a4.jpg">
                                </a>
                                <div class="media-body text-right ">
                                    <small class="pull-left">5m ago</small>
                                    <strong>John Smith</strong>
                                    <p class="m-b-xs">
                                        Lorem Ipsum is simply dummy text of the printing.
                                    </p>
                                    <small class="text-muted">Today 4:21 pm - 12.06.2014</small>
                                </div>
                            </div>-->
                        </div>
                    </div>
                    <div class="chat-form">
                        <form role="form">
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message" id="messageUser"></textarea>
                            </div>
                            <div class="text-right">
                                <button onclick="addComment(<?php echo $proj_id;?>)" type="button" class="btn btn-sm btn-primary m-t-n-xs"><strong>Send message</strong></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include("footer.php"); ?>
<script src="<?php echo base_url();?>assets/js/comments.js"></script>
<script>
    getProjects(<?php echo $proj_id;?>,<?php echo $session['proj_emp_id']; ?>);
</script>



