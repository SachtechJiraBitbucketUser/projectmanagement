<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/28/2017
 * Time: 7:07 PM
 */

class Comments extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/dashboard',array("page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    public function view($proj_id)
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/comments',array('proj_id'=>$proj_id,"page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }

    public function getComments()
    {
        $requiredfields = array('proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $where = array('comm_proj_id'=>$this->input->post('proj_id'),"comm_status"=>"active");
        $type_data = $this->dbresults->get_data('tbl_comments','*',$where);
        if($type_data) {

            for($p = 0; $p < count($type_data); $p++){
                $emp_id = $type_data[$p]->comm_emp_id;
                $empData = $this->getEmpName($emp_id);
                $emp_name = "";$emp_img="";
                if($empData != ""){
                    $emp_name =  $empData[0]->emp_name;
                    $emp_img =  $empData[0]->emp_img;
                }
                $type_data[$p]->emp_name = $emp_name;
                $type_data[$p]->emp_img = $emp_img;
            }
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Comments of Project  found';
            $response["data"] = $type_data;

            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Comments of Project not found';
        return $this->validator->apiResponse($response);
    }
    public function getEmpName($emp_id){
        $empName = $this->dbresults->get_data('tbl_employees','emp_id,emp_name,emp_type,emp_img',array("emp_id"=>$emp_id));
        if($empName){
            return $empName;
        }
        return "";
    }

    public function addComments() {
        $requiredfields = array('proj_id','proj_emp_id','messageUser','currentDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $data = array('comm_proj_id'=>$this->input->post('proj_id'),
            'comm_emp_id'=>$this->input->post('proj_emp_id'),
            'comm_text'=>$this->input->post('messageUser'),
            'comm_status'=>'active',
            'comm_created_at'=>$this->input->post('currentDate'));

        $ins_ = $this->dbresults->post_data('tbl_comments',$data);
        if($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Comment created successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to create comment type';
        return $this->validator->apiResponse($response);
    }
}