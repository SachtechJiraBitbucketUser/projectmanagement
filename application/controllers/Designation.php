<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/12/2018
 * Time: 11:44 AM
 */

class Designation extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/designation',array("page_name"=>"designation"));
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    public function createDesignation() {
        $requiredfields = array('emp_designation','created_at');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $data = array('desig_title'=>$this->input->post('emp_designation'),
                    'desi_created_at'=>$this->input->post('created_at'));
        $ins_ = $this->dbresults->post_data('tbl_designation',$data);
        if($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Designation created successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to create Designation';
        return $this->validator->apiResponse($response);
    }
    public function getAllDesignation() {
        $response = array();
        $type_data = $this->dbresults->get_data('tbl_designation','*',array());
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Designation data found';
            $response["data"] = $type_data;
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Designation not found';
        return $this->validator->apiResponse($response);
    }

    public function designation_update() {
        $requiredfields = array('desig_id','desi_title');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $data = array('desig_title'=>$this->input->post('desi_title'));
        $where = array('desig_id'=>$this->input->post('desig_id'));
        $update_ = $this->dbresults->update_data('tbl_designation',$data,$where);

        if($update_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Designation updated successfully';
            return $this->validator->apiResponse($response);

        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to update designation';
        return $this->validator->apiResponse($response);
    }
    public function removeDestination() {
        $requiredfields = array('desig_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $where = array('desig_id'=>$this->input->post('desig_id'));
        $remove_ = $this->dbresults->delete_data('tbl_designation',$where);
        if($remove_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Designation removed successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to remove designation';
        return $this->validator->apiResponse($response);
    }
}