<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/29/2017
 * Time: 5:44 PM
 */

class Logout extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function signout(){
        $this->session->sess_destroy();
        redirect('user/', 'refresh');
    }
}