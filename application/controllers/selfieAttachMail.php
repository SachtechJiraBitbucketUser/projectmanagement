<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/17/2018
 * Time: 5:40 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class selfieAttachMail extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->library('validator');
    }
    public function sendOtp(){
        $requiredfields = array('emp_email');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $emp_email = $this->input->post('emp_email');
        $user_data = $this->dbresults->get_data('tbl_employees','emp_email',array("emp_email"=>$emp_email));
        if(!$user_data) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Invalid user, please fill correct email address';
            return $this->validator->apiResponse($response);
        }
        else{
            $otp = $this->generateRandomString();
            $bodyData  = '<p>Please fill this otp for change password : <span style="color: #1AB394;">'.$otp.'</span></p>';
            $sendEmail = $this->dbresults->send_mail($emp_email,$bodyData,"STS PM Tool");
            if($sendEmail){
                $dataUser  = array('emp_otp'=>$otp);
                $where = array('emp_email'=>$this->input->post('emp_email'));
                $update_user = $this->dbresults->update_data('tbl_employees',$dataUser,$where);
                if($update_user){
                    $response[$this->config->item('status')] = true;
                    $response[$this->config->item('message')] = 'Otp send to your email address.';
                    return $this->validator->apiResponse($response);
                }
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = 'error in update user related data';
                return $this->validator->apiResponse($response);
            }
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in otp send';
            $response[$this->config->item('mailErr')] = $sendEmail;
            return $this->validator->apiResponse($response);
        }
    }
    public function userImageUpdate(){
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $config['upload_path']      = FCPATH . 'assets/img/userImg/';
        $config['allowed_types']    = '*';
        $config['max_size']         = 2048;
        $config['encrypt_name']     = TRUE;
        $config['max_size']         = '300000'; // Added Max Size
        $config['overwrite']        = TRUE;
        $config['max_width']        = 0;
        $config['max_height']       = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if( ! is_dir($config['upload_path'])) {
            @mkdir($config['upload_path'], 0755, true);
        }
        $name = "user_image_file"; // Field name
        if( ! $this->upload->do_upload($name)) {
            $error = array('error' => $this->upload->display_errors());
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in image upload file';
            $response['imageData'] = $error;
            return $this->validator->apiResponse($response);
        }
        else {
            $data = array('upload_data' => $this->upload->data());
            $imageData = $this->upload->data();
            $imgName = "";
            foreach($imageData as $key => $val){
                if($key == "file_name"){
                    $imgName = $val;
                }
            }
            $dataUser  = array('emp_img'=>$imgName);
            $whereUser = array('emp_id'=>$this->input->post('emp_id'));
            $update_user = $this->dbresults->update_data('tbl_employees',$dataUser,$whereUser);
            if($update_user){
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = 'profile update successfully';
                $response['data'] = $data;
                return $this->validator->apiResponse($response);
            }
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in profile update';
            $response['data'] = $data;
            return $this->validator->apiResponse($response);
        }
    }
    public function selfieMailAttach(){
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $config['upload_path']      = FCPATH . 'assets/img/userImg/';
        $config['allowed_types']    = '*';
        $config['max_size']         = 2048;
        $config['encrypt_name']     = TRUE;
        $config['max_size']         = '300000'; // Added Max Size
        $config['overwrite']        = TRUE;
        $config['max_width']        = 0;
        $config['max_height']       = 0;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if( ! is_dir($config['upload_path'])) {
            @mkdir($config['upload_path'], 0755, true);
        }
        $name = "user_image_file"; // Field name
        if( ! $this->upload->do_upload($name)) {
            $error = array('error' => $this->upload->display_errors());
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in image upload file';
            $response['imageData'] = $error;
            return $this->validator->apiResponse($response);
        }
        else {
            $data = array('upload_data' => $this->upload->data());
            $imageData = $this->upload->data();
            $imgName = "";
            foreach($imageData as $key => $val){
                if($key == "file_name"){
                    $imgName = $val;
                }
            }
            $dataUser  = array('emp_img'=>$imgName);
            $whereUser = array('emp_id'=>$this->input->post('emp_id'));
            $update_user = $this->dbresults->update_data('tbl_employees',$dataUser,$whereUser);
            if($update_user){
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = 'profile update successfully';
                $response['data'] = $data;
                return $this->validator->apiResponse($response);
            }
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in profile update';
            $response['data'] = $data;
            return $this->validator->apiResponse($response);
        }
    }


}