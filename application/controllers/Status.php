<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 28-12-2017
 * Time: 20:48
 */
class Status extends CI_Controller
{
 public function __construct() {
   parent::__construct();
 }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/status',array("page_name"=>"status"));
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    public function getAllStatus() {
        $response = array();
        $type_data = $this->dbresults->get_data('tbl_project_status','*',array());
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Project status data found';
            $response["data"] = $type_data;
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Project status  data not found';
        return $this->validator->apiResponse($response);
    }
    public function createProjectStatus() {
        $requiredfields = array('proj_status','created_at');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $data = array('status_title'=>$this->input->post('proj_status'),
            'status_created_at'=>$this->input->post('created_at'));
        $ins_ = $this->dbresults->post_data('tbl_project_status',$data);
        if($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Status created successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to create project Status';
        return $this->validator->apiResponse($response);
    }
    public function status_update() {
        $requiredfields = array('statusId','proj_status');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $data = array('status_title'=>$this->input->post('proj_status'));
        $where = array('status_id'=>$this->input->post('statusId'));
        $update_ = $this->dbresults->update_data('tbl_project_status',$data,$where);

        if($update_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'project status updated successfully';
            return $this->validator->apiResponse($response);

        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to update project status';
        return $this->validator->apiResponse($response);
    }
    public function removeStatus() {
        $requiredfields = array('status_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $where = array('status_id'=>$this->input->post('status_id'));
        $remove_ = $this->dbresults->delete_data('tbl_project_status',$where);
        if($remove_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'project status removed successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to remove project status';
        return $this->validator->apiResponse($response);
    }
}