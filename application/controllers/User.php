<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->library('validator');
    }
    public function index()
    {
        $this->load->view('backend/login');
    }
    public function users() {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/employees',array("page_name"=>"employees"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }
    public function profileData() {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $desig_data = $this->dbresults->get_data('tbl_designation','desig_id,desig_title',array());
            $this->load->view('backend/header',array('session'=>$this->session->userdata(),"desig_data"=>$desig_data));
            $this->load->view('backend/profile',array("page_name"=>"myprofile"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }
    function generateRandomString($length = 5) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function forgot() {
        $this->load->view('backend/forgot_password');
    }
    public function sendOtp(){
        $requiredfields = array('emp_email');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $emp_email = $this->input->post('emp_email');
        $user_data = $this->dbresults->get_data('tbl_employees','emp_email',array("emp_email"=>$emp_email));
        if(!$user_data) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Invalid user, please fill correct email address';
            return $this->validator->apiResponse($response);
        }
        else{
            $otp = $this->generateRandomString();
            $bodyData  = '<p>Please fill this otp for change password : <span style="color: #1AB394;">'.$otp.'</span></p>';
            $sendEmail = $this->dbresults->send_mail($emp_email,$bodyData,"STS PM Tool");
            if($sendEmail){
                $dataUser  = array('emp_otp'=>$otp);
                $where = array('emp_email'=>$this->input->post('emp_email'));
                $update_user = $this->dbresults->update_data('tbl_employees',$dataUser,$where);
                if($update_user){
                    $response[$this->config->item('status')] = true;
                    $response[$this->config->item('message')] = 'Otp send to your email address.';
                    return $this->validator->apiResponse($response);
                }
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = 'error in update user related data';
                return $this->validator->apiResponse($response);
            }
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in otp send';
            $response[$this->config->item('mailErr')] = $sendEmail;
            return $this->validator->apiResponse($response);
        }
    }
    public function otpValidUser(){
        $requiredfields = array('emp_email','otp','newPass');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $emp_email = $this->input->post('emp_email');
        $otp       = $this->input->post('otp');
        $newPass       = $this->input->post('newPass');
        $newPass = md5($newPass);
        $user_data = $this->dbresults->get_data('tbl_employees','emp_email,emp_otp',array("emp_email"=>$emp_email,
                                                                                          "emp_otp"=>$otp));
        if(!$user_data) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Invalid data, please fill correct email address and otp.';
            return $this->validator->apiResponse($response);
        }
        else{
            $update_user = $this->dbresults->update_data('tbl_employees',array('emp_password'=>$newPass),
                array('emp_email'=>$this->input->post('emp_email')));
            if($update_user){
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = 'Password reset successfully, Now you can login.';
                return $this->validator->apiResponse($response);
            }
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'error in update user related data';
            return $this->validator->apiResponse($response);
        }
    }
    public function getPartUser() {
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $userInfo = $this->dbresults->get_data('tbl_employees','emp_name,emp_designation,emp_email,emp_mobile,
        emp_message,emp_img',array("emp_id"=>$this->input->post('emp_id')));
        if($userInfo){
            $desigId = $userInfo[0]->emp_designation;
            $designation = $this->getDesig($desigId)[0]->desig_title;
            $userInfo[0]->designation = $designation;
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'data found';
            $response['userInfo'] = $userInfo;
            return $this->validator->apiResponse($response);
        }
        else{
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'data not found';
            return $this->validator->apiResponse($response);
        }
    }
    public function userInfoUpdate(){
        $requiredfields = array('emp_id','user_name','user_email','user_designation');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $dataUser  = array('emp_name'=>$this->input->post('user_name'),
                     'emp_email'=>$this->input->post('user_email'),
                     'emp_designation'=>$this->input->post('user_designation'),
                     'emp_mobile'=>$this->input->post('user_mobile'),
                     'emp_message'=>$this->input->post('user_message'));
        $where = array('emp_id'=>$this->input->post('emp_id'));
        $update_user = $this->dbresults->update_data('tbl_employees',$dataUser,$where);
        if ($update_user ) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Information update successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'error in update Information';
        return $this->validator->apiResponse($response);

    }
    public function userImageUpdate(){
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
            $config['upload_path']      = FCPATH . 'assets/img/userImg/';
            $config['allowed_types']    = '*';
            $config['max_size']         = 2048;
            $config['encrypt_name']     = TRUE;
            $config['max_size']         = '300000'; // Added Max Size
            $config['overwrite']        = TRUE;
            $config['max_width']        = 0;
            $config['max_height']       = 0;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if( ! is_dir($config['upload_path'])) {
                @mkdir($config['upload_path'], 0755, true);
            }
            $name = "user_image_file"; // Field name
            if( ! $this->upload->do_upload($name)) {
                $error = array('error' => $this->upload->display_errors());
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = 'error in image upload file';
                $response['imageData'] = $error;
                return $this->validator->apiResponse($response);
            }
            else {
                $data = array('upload_data' => $this->upload->data());
                $imageData = $this->upload->data();
                $imgName = "";
                foreach($imageData as $key => $val){
                    if($key == "file_name"){
                        $imgName = $val;
                    }
                }
                $dataUser  = array('emp_img'=>$imgName);
                $whereUser = array('emp_id'=>$this->input->post('emp_id'));
                $update_user = $this->dbresults->update_data('tbl_employees',$dataUser,$whereUser);
                if($update_user){
                    $response[$this->config->item('status')] = true;
                    $response[$this->config->item('message')] = 'profile update successfully';
                    $response['data'] = $data;
                    return $this->validator->apiResponse($response);
                }
                $response[$this->config->item('status')] = false;
                $response[$this->config->item('message')] = 'error in profile update';
                $response['data'] = $data;
                return $this->validator->apiResponse($response);
            }
    }
    public function getFieldsData(){
        $response = array();
        $type_data = $this->dbresults->get_data('tbl_project_type','*',array());
        $desig_data = $this->dbresults->get_data('tbl_designation','*',array());
        $response["typeData"] = $type_data;
        $response["desig_data"] = $desig_data;
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'data found';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'data not found';
        return $this->validator->apiResponse($response);
    }
    public function login() {
        $requiredfields = array('email_id','password');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        if(!$this->validator->isValidEmail($this->input->post('email_id'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Please enter valid email-address';
            $this->validator->apiResponse($response);
            return false;
        }
        $this->users_model->setEmpEmail($this->input->post('email_id'));
        $this->users_model->setEmpPassword($this->input->post('password'));
        $isLogin = $this->users_model->login();
        if(!$isLogin) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Invalid User, Please enter valid email-id and password';
            $this->validator->apiResponse($response);
            return false;
        }
        $this->session->set_userdata(array(
            'proj_emp_id'  => $isLogin[0]->emp_id,
            'proj_emp_name' => $isLogin[0]->emp_name,
            'proj_emp_email'  => $isLogin[0]->emp_email,
            'proj_emp_type'  => $isLogin[0]->emp_type,
            'proj_is_logged'     => true,
        ));
        $response[$this->config->item('status')] = true;
        $response[$this->config->item('message')] = 'User logged in successfully';
        $this->validator->apiResponse($response);
        return false;
    }
    public function users_data() {
      $usersData = $this->users_model->getAllUsers();
      if($usersData) {
          $response[$this->config->item('status')] = true;
          $response[$this->config->item('message')] = 'User data found';
          for($i = 0; $i < count($usersData); $i++){
            $emp_designation = $this->getDesig($usersData[$i]->emp_designation)[0]->desig_title;
            $usersData[$i]->emp_designation_name = $emp_designation;
          }
          $response["data"] = $usersData;
          return $this->validator->apiResponse($response);
      }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'User data not found';
        return $this->validator->apiResponse($response);

    }
    function getType($typeId) {
        $typeName = $this->dbresults->get_data('tbl_project_type','type_id,type_name',array("type_id"=>$typeId));
        if($typeName){
            return $typeName;
        }
        return "";
    }
    function getDesig($desigId) {
        $desigName = $this->dbresults->get_data(' tbl_designation','desig_id,desig_title',array("desig_id"=>$desigId));
        if($desigName){
            return $desigName;
        }
        return "";
    }
    public function user_create() {
        $requiredfields = array('emp_name','emp_email','emp_password','emp_desg','emp_created_at','emp_status','emp_type');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }

        if(!$this->validator->isValidEmail($this->input->post('emp_email'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Please enter valid email-id';
            return $this->validator->apiResponse($response);
        }
        if($this->isEmailExists($this->input->post('emp_email'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Email id already exists';
            return $this->validator->apiResponse($response);
        }
        if($this->isNameExists($this->input->post('emp_name'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'This name is already exists';
            return $this->validator->apiResponse($response);
        }
        $this->users_model->setEmpName($this->input->post('emp_name'));
        $this->users_model->setEmpEmail($this->input->post('emp_email'));
        $this->users_model->setEmpPassword($this->input->post('emp_password'));
        $this->users_model->setEmpCreatedAt($this->input->post('emp_created_at'));
        $this->users_model->setEmpDesignation($this->input->post('emp_desg'));
        $this->users_model->setEmpStatus($this->input->post('emp_status'));
        $this->users_model->setEmpType($this->input->post('emp_type'));

        if($this->users_model->save()) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee created successfully';
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Unable to create employee';
        return $this->validator->apiResponse($response);
    }
    public function isEmailExists($emp_email){
        $where = array('emp_email'=>$emp_email);
        $user_data = $this->dbresults->get_data('tbl_employees','*',$where);
        if($user_data){
            return true;
        }
        return false;
    }
    public function isNameExists($emp_name){
        $where = array('emp_name'=>$emp_name);
        $user_data = $this->dbresults->get_data('tbl_employees','*',$where);
        if($user_data){
            return true;
        }
        return false;
    }
    public function isNameExistUpdate($emp_id,$emp_name){
        $where = array('emp_name'=>$emp_name,'emp_id !='=>$emp_id);
        $user_data = $this->dbresults->get_data('tbl_employees','*',$where);
        if($user_data){
            return true;
        }
        return false;
    }
    public function getEmployeeList() {
        $requiredfields = array('emp_role');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $where = array('emp_role'=>$this->input->post('emp_role'));
        if($this->input->post('emp_role') == "All"){
            $where = array();
        }
        $type_data = $this->dbresults->get_data('tbl_employees','*',$where);
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee List found';
            $response["data"] = $type_data;
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Employee List not  found';
        return $this->validator->apiResponse($response);
    }
    public function user_update() {
        $requiredfields = array('emp_name','emp_desg','emp_status');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        if(!$this->validator->isValidEmail($this->input->post('emp_email'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Please enter valid email-id';
            return $this->validator->apiResponse($response);
        }
        if($this->isNameExistUpdate($this->input->post('emp_id'),$this->input->post('emp_name'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'This name is allready exist.';
            return $this->validator->apiResponse($response);
        }
        $this->users_model->setEmpName($this->input->post('emp_name'));
        $this->users_model->setEmpDesignation($this->input->post('emp_desg'));
        $this->users_model->setEmpStatus($this->input->post('emp_status'));
        $this->users_model->setEmpId($this->input->post('emp_id'));

        if($this->users_model->update()) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee updated successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Unable to update employee';
        return $this->validator->apiResponse($response);
    }
    public function user_remove() {
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $this->users_model->setEmpId($this->input->post('emp_id'));
        if($this->users_model->remove())  {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee removed successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Unable to remove employee';
        return $this->validator->apiResponse($response);
    }
}
