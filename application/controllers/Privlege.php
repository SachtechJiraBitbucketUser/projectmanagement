<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/1/2018
 * Time: 5:06 PM
 */

class Privlege extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('privlege_model');
        $this->load->library('validator');
    }

    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/privlege',array("page_name"=>"privilege"));
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    function getDesig($desigId) {
        $desigName = $this->dbresults->get_data(' tbl_designation','desig_id,desig_title',array("desig_id"=>$desigId));
        if($desigName){
            return $desigName;
        }
        return "";
    }
    public function users_data() {
        $usersData = $this->privlege_model->getAllUsers();
        if($usersData['Status'] == "Success") {
            $usersData = $usersData['UserData'];
            for($i=0;$i<count($usersData); $i++){
                $emp_designation = $this->getDesig($usersData[$i]->emp_designation);
                $usersData[$i]->designation = $emp_designation[0]->desig_title;
            }
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'User data found';
            $response["data"] = $usersData;
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'User data not found';
        return $this->validator->apiResponse($response);
    }
    function getType($typeId) {
        $typeName = $this->dbresults->get_data('tbl_project_type','type_id,type_name',array("type_id"=>$typeId));
        if($typeName){
            return $typeName;
        }
        return "";
    }
    public function get_privlege() {
        $response = array();
        $menu_data = $this->dbresults->get_data('tbl_menu','*',array());
        $user_data = $this->dbresults->get_data('tbl_employees','*',array('emp_id'=>$this->input->post('emp_id')));
        if($menu_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Menu data found';
            $response["data"] = $menu_data;
            $response["userData"] = $user_data;
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Menu data not found';
        return $this->validator->apiResponse($response);
    }
    public function createPrivlege() {
        $response = array();
        $type_data = $this->dbresults->get_data('tbl_employees','*',array('emp_id'=>$this->input->post('emp_id')));
        if($type_data) {
            /* in user exist*/
            $requiredfields = array('emp_id','privlegeValue');
            $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
            $response = array();
            $status = $param_response[$this->config->item('status')];
            if (!$status ) {
                return $this->validator->apiResponse($param_response);
            }
            $data = array('emp_menu_privlege'=>$this->input->post('privlegeValue'));
            $where = array('emp_id'=>$this->input->post('emp_id'));
            $update_ = $this->dbresults->update_data('tbl_employees',$data,$where);
            if($update_) {
                $response[$this->config->item('status')] = true;
                $response[$this->config->item('message')] = 'Privlege updated successfully';
                return $this->validator->apiResponse($response);
            }
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'unable to update Privlege';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to give Privlege';
        return $this->validator->apiResponse($response);
    }
}