<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/3/2018
 * Time: 5:59 PM
 */

class CreateProject extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProjectModal');
        $this->load->library('validator');
    }
    public function view()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/createProject',array("page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }
    public function editProject($proj_id)
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/editProject',array('proj_id'=>$proj_id,"page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }

    public function getFieldsData(){
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $type_data = $this->dbresults->get_data('tbl_project_type','type_id,type_name,type_status',array());
        $status_data = $this->dbresults->get_data('tbl_project_status','status_id,status_title',array());
        $emp_developer_data = $this->dbresults->get_data('tbl_employees','emp_id,emp_name,emp_status',array());
        $projMaxNum = $this->ProjectModal->getProjMaxNum();
        $response["typeData"] = $type_data;
        $response["statusData"] = $status_data;
        $response["empDevData"] = $emp_developer_data;
        $response["projMaxNum"] = $projMaxNum;
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'data found';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'data not found';
        return $this->validator->apiResponse($response);
    }
    public function createProject(){
        $requiredfields = array('projectType','projectName','projectStatus','projectManager',
            'startDate','projectComment','startDate','deadlineDate','currentDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjId($this->input->post('projectId'));
        $this->ProjectModal->setEmpId($this->input->post('emp_id'));
        $this->ProjectModal->setProjType($this->input->post('projectType'));
        $this->ProjectModal->setProjName($this->input->post('projectName'));
        $this->ProjectModal->setProjStatus($this->input->post('projectStatus'));
        $this->ProjectModal->setProjManager($this->input->post('projectManager'));
        $this->ProjectModal->setProjDeveloper($this->input->post('projectTeamDeveloper'));
        $this->ProjectModal->setCommText($this->input->post('projectComment'));
        $this->ProjectModal->setProjStartDate($this->input->post('startDate'));
        $this->ProjectModal->setProjDeadLine($this->input->post('deadlineDate'));
        $this->ProjectModal->setProjClosingDate($this->input->post('closingDate'));
        $this->ProjectModal->setMileValue($this->input->post('milestoneValues'));

        if($this->input->post('amountType') != ""){
            $this->ProjectModal->setProjAmountType($this->input->post('amountType'));
            if($this->input->post('amountType') == "fixedPrice"){
                $this->ProjectModal->setProjTotalAmount($this->input->post('totalProjCost'));
            }
            else if($this->input->post('amountType') == "timeMaterial"){
                $this->ProjectModal->setProjTotalEmp($this->input->post('totalProjMember'));
                $this->ProjectModal->setProjHourlyRate($this->input->post('totalProjHours'));
                $this->ProjectModal->setProjTotalAmount($this->input->post('totalProjCost'));
            }
        }
        $this->ProjectModal->setCurrentDate($this->input->post('currentDate'));
        $isCreate = $this->ProjectModal->createProject();
        $status = $isCreate['Status'];
        if(!$status) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = $isCreate['Message'];
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = $isCreate['Message'];
        $response['proj_id'] = $isCreate['proj_id'];
        $this->validator->apiResponse($response);
        return true;
    }
    public function updateProject(){
        $requiredfields = array('proj_id','projectType','projectName','projectStatus','projectManager',
            'startDate','deadlineDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjId($this->input->post('proj_id'));
        $this->ProjectModal->setEmpId($this->input->post('emp_id'));
        $this->ProjectModal->setProjType($this->input->post('projectType'));
        $this->ProjectModal->setProjName($this->input->post('projectName'));
        $this->ProjectModal->setProjStatus($this->input->post('projectStatus'));
        $this->ProjectModal->setProjManager($this->input->post('projectManager'));
        $this->ProjectModal->setProjDeveloper($this->input->post('projectTeamDeveloper'));
        $this->ProjectModal->setProjStartDate($this->input->post('startDate'));
        $this->ProjectModal->setProjDeadLine($this->input->post('deadlineDate'));
        $this->ProjectModal->setProjClosingDate($this->input->post('closingDate'));

        if($this->input->post('amountType') != ""){
            $this->ProjectModal->setProjAmountType($this->input->post('amountType'));
            if($this->input->post('amountType') == "fixedPrice"){
                $this->ProjectModal->setProjTotalAmount($this->input->post('totalProjCost'));
            }
            else if($this->input->post('amountType') == "timeMaterial"){
                $this->ProjectModal->setProjTotalEmp($this->input->post('totalProjMember'));
                $this->ProjectModal->setProjHourlyRate($this->input->post('totalProjHours'));
                $this->ProjectModal->setProjTotalAmount($this->input->post('totalProjCost'));
            }
        }
        $isCreate = $this->ProjectModal->updateProject();
        $status = $isCreate['Status'];
        if(!$status) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = $isCreate['Message'];
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = $isCreate['Message'];
        $this->validator->apiResponse($response);
        return true;
    }
}