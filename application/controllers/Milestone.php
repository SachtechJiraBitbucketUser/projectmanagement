<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/29/2017
 * Time: 12:20 PM
 */

class Milestone extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/dashboard',array("page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    public function view($proj_id)
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/milestone',array('proj_id'=>$proj_id,"page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }
    public function addMilestone() {
        $requiredfields = array('proj_id','proj_emp_id','milestoneName','milestoneDescription','startDate','deadlineDate',
            'currentDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();

        $status = $param_response[$this->config->item('status')];

        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $data = array('mile_project_id'=>$this->input->post('proj_id'),
            'mile_emp_id'=>$this->input->post('proj_emp_id'),
            'mile_name'=>$this->input->post('milestoneName'),
            'mile_description'=>$this->input->post('milestoneDescription'),
            'mile_start_date'=>$this->input->post('startDate'),
            'mile_end_date'=>$this->input->post('deadlineDate'),
            'mile_status'=>'active',
            'mile_created_at'=>$this->input->post('currentDate'));

        $ins_ = $this->dbresults->post_data('tbl_milestone',$data);
        if($ins_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Milestone created successfully';
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to create Milestone type';
        return $this->validator->apiResponse($response);
    }
    public function getMilestone()
    {
        $requiredfields = array('proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $where = array('mile_project_id'=>$this->input->post('proj_id'),"mile_status"=>"active");
        $type_data = $this->dbresults->get_data('tbl_milestone','*',$where);
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Milestone of Project data found';
            $response["data"] = $type_data;
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Milestone of Project data not found';
        return $this->validator->apiResponse($response);
    }

    public function deleteMilestone(){
        $requiredfields = array('mile_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $dataMilestone  = array('mile_status'=>"delete");
        $whereMilestone = array('mile_id'=>$this->input->post('mile_id'));
        $update_comment = $this->dbresults->update_data('tbl_milestone',$dataMilestone,$whereMilestone);
        if($update_comment) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Milestone delete successfully';
            return $this->validator->apiResponse($response);

        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to delete milestone';

        return $this->validator->apiResponse($response);
    }
    public function updateMilestone(){
        $requiredfields = array('mile_id','proj_emp_id','milestoneName','milestoneDescription','startDate','deadlineDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();

        $status = $param_response[$this->config->item('status')];

        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }

        $data = array('mile_emp_id'=>$this->input->post('proj_emp_id'),
            'mile_name'=>$this->input->post('milestoneName'),
            'mile_start_date'=>$this->input->post('startDate'),
            'mile_end_date'=>$this->input->post('deadlineDate'),
            'mile_description'=>$this->input->post('milestoneDescription'));
        $where = array('mile_id'=>$this->input->post('mile_id'));

        $update_ = $this->dbresults->update_data('tbl_milestone',$data,$where);

        if($update_) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Milestone updated successfully';
            return $this->validator->apiResponse($response);

        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'unable to update Milestone';
        return $this->validator->apiResponse($response);
    }
}