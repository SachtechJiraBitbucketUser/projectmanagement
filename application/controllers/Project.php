<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProjectModal');
        $this->load->library('validator');
    }
    public function index($status_value)
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/project',array("status_value"=>$status_value,"page_name"=>"projects"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }
    /*get menu according to user*/
    public function getAllMenu(){
        $response = array();
        $menuData = array();
        $type_data = $this->dbresults->get_data('tbl_employees','*',array("emp_id"=>$this->input->post('emp_id')));
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee data found';
            $response["data"] = $type_data;
            $menuId = $type_data[0]->emp_menu_privlege;
            if (strpos($menuId, ',') !== false) {
                $menuId = explode(",",$menuId);
                for($k = 0; $k < count($menuId); $k++){
                    $menuRow = $this->menuName($menuId[$k]);
                    array_push($menuData,$menuRow);
                }
            }
            $response['menuData'] = $menuData;
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'employee data not found';
        return $this->validator->apiResponse($response);
    }
    public function menuName($menuId){
        $menuRow = $this->dbresults->get_data('tbl_menu','*',array("id"=>$menuId));
        return $menuRow[0];
    }
    public function getAllProjects(){
        $projectData = $this->ProjectModal->getProjects();
        if($projectData){
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Data Found Successfully';
            $response['AllProject'] = $projectData;
        }
        else{
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Error in data found';
        }
        $this->validator->apiResponse($response);
    }
    /*get project type and employee data*/
    public function get_project_employee(){
        $projectTypeData = $this->ProjectModal->getProjectType();
        $employeeData = $this->ProjectModal->getEmployee();
        if($projectTypeData){
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Data Found Successfully';
            $response['allProjectType'] = $projectTypeData;
            $response['allEmployee'] = $employeeData;
        }
        else{
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Error in data found';
        }
        $this->validator->apiResponse($response);
    }
}
