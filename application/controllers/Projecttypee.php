<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 28-12-2017
 * Time: 12:34
 */
class ProjectTypee extends CI_Controller
{
 public function __construct()
 {
     parent::__construct();
     $this->load->model('ProjectModal');
     $this->load->library('validator');
 }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/type');
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    public function getAllMenu(){
        $response = array();
        $menuData = array();
        $type_data = $this->dbresults->get_data('tbl_employees','*',array("emp_id"=>$this->input->post('emp_id')));
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee data found';
            $response["data"] = $type_data;
            $menuId = $type_data[0]->emp_menu_privlege;
            if (strpos($menuId, ',') !== false) {
                $menuId = explode(",",$menuId);
                for($k = 0; $k < count($menuId); $k++){
                    $menuRow = $this->menuName($menuId[$k]);
                    array_push($menuData,$menuRow);
                }
            }
            $response['menuData'] = $menuData;
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'employee data not found';
        return $this->validator->apiResponse($response);
    }
 /*
  * this will get all types data
  *
  * */
 public function types() {
   $response = array();
   $type_data = $this->dbresults->get_data('tbl_project_type','*',array());
   if($type_data) {
       $response[$this->config->item('status')] = true;
       $response[$this->config->item('message')] = 'Type data found';
       $response["data"] = $type_data;
       return $this->validator->apiResponse($response);
   }

     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'Type data not found';
     return $this->validator->apiResponse($response);
 }

    /*
     * this will get create new type data
     *
     * */

    public function type_create() {
     $requiredfields = array('type_name','type_status','type_desc','type_created_at');
     $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
     $response = array();

     $status = $param_response[$this->config->item('status')];

     if (!$status ) {
         return $this->validator->apiResponse($param_response);
     }
     $data = array('type_name'=>$this->input->post('type_name'),
                'type_status'=>$this->input->post('type_status'),
                'type_created_at'=>$this->input->post('type_created_at'),
                'type_desc'=>$this->input->post('type_desc'));

     $ins_ = $this->dbresults->post_data('tbl_project_type',$data);
     if($ins_) {
         $response[$this->config->item('status')] = true;
         $response[$this->config->item('message')] = 'Type created successfully';
         return $this->validator->apiResponse($response);
     }
     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'unable to create project type';
     return $this->validator->apiResponse($response);
 }
 /*
 * this will update type data
 *
 *
 * */

    public function type_update() {
     $requiredfields = array('type_id','type_name','type_status','type_desc');
     $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
     $response = array();

     $status = $param_response[$this->config->item('status')];

     if (!$status ) {
         return $this->validator->apiResponse($param_response);
     }

     $data = array('type_name'=>$this->input->post('type_name'),
               'type_status'=>$this->input->post('type_status'),
               'type_desc'=>$this->input->post('type_desc'));
     $where = array('type_id'=>$this->input->post('type_id'));

     $update_ = $this->dbresults->update_data('tbl_project_type',$data,$where);

     if($update_) {
         $response[$this->config->item('status')] = true;
         $response[$this->config->item('message')] = 'project type updated successfully';
         return $this->validator->apiResponse($response);

     }
     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'unable to update project type';
     return $this->validator->apiResponse($response);
 }

 /*
 * this will remove type data
 *
 *
 * */
 public function type_remove() {
     $requiredfields = array('type_id');
     $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
     $response = array();

     $status = $param_response[$this->config->item('status')];

     if (!$status ) {
         return $this->validator->apiResponse($param_response);
     }
     $where = array('type_id'=>$this->input->post('type_id'));

     $remove_ = $this->dbresults->delete_data('tbl_project_type',$where);
     if($remove_) {
         $response[$this->config->item('status')] = true;
         $response[$this->config->item('message')] = 'project type removed successfully';
         return $this->validator->apiResponse($response);

     }
     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'unable to remove project type';

     return $this->validator->apiResponse($response);
 }

}