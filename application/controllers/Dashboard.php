<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProjectModal');
        $this->load->library('validator');
    }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');
        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/dashboard',array("page_name"=>"dashboard"));
        }
        else{
            redirect('user/', 'refresh');
        }
    }
    public function getProjectCount(){
        $totalProj = 0;
        $newProj    =   0;
        $liveStabilizationProj  =   0;
        $liveClosedProj =   0;
        $onHoldProj =   0;
        $archivedProj   =   0;
        $assignedProj=0;

        $totalProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("status"=>"active"));
        $newProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("proj_status"=>5,"status"=>"active"));
        $liveStabilizationProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("proj_status"=>6,"status"=>"active"));
        $liveClosedProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("proj_status"=>7,"status"=>"active"));
        $onHoldProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("proj_status"=>8,"status"=>"active"));
        $archivedProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("proj_status"=>9,"status"=>"active"));
        $assignedProjQry = $this->dbresults->get_data('tbl_projects','proj_id',array("proj_status"=>10,"status"=>"active"));
        if($totalProjQry){
            $totalProj = count($totalProjQry);
        }if($newProjQry){
            $newProj = count($newProjQry);
        }if($liveStabilizationProjQry){
            $liveStabilizationProj = count($liveStabilizationProjQry);
        }if($liveClosedProjQry){
            $liveClosedProj = count($liveClosedProjQry);
        }if($onHoldProjQry){
            $onHoldProj = count($onHoldProjQry);
        }if($archivedProjQry){
            $archivedProj = count($archivedProjQry);
        }if($assignedProjQry){
            $assignedProj = count($assignedProjQry);
        }
        $response[$this->config->item('status')] = true;
        $response[$this->config->item('message')] = "project count found successfully";
        $response['totalProj'] = $totalProj;
        $response['newProj'] = $newProj;
        $response['liveStabilizationProj'] = $liveStabilizationProj;
        $response['liveClosedProj'] = $liveClosedProj;
        $response['onHoldProj'] = $onHoldProj;
        $response['archivedProj'] = $archivedProj;
        $response['assignedProj'] = $assignedProj;
        $this->validator->apiResponse($response);
        return false;
    }
    /*get menu according to user*/
    public function getAllMenu(){
        $response = array();
        $menuData = array();
        $type_data = $this->dbresults->get_data('tbl_employees','*',array("emp_id"=>$this->input->post('emp_id')));
        if($type_data) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee data found';
            $response["data"] = $type_data;
            $menuId = $type_data[0]->emp_menu_privlege;
            if (strpos($menuId, ',') !== false) {
                $menuId = explode(",",$menuId);
                for($k = 0; $k < count($menuId); $k++){
                    $menuRow = $this->menuName($menuId[$k]);
                    array_push($menuData,$menuRow);
                }
            }
            $response['menuData'] = $menuData;
            return $this->validator->apiResponse($response);
        }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'employee data not found';
        return $this->validator->apiResponse($response);
    }
    public function menuName($menuId){
        $menuRow = $this->dbresults->get_data('tbl_menu','*',array("id"=>$menuId));
        return $menuRow[0];
    }
    public function getParticularProject(){
        $requiredfields = array('emp_id','proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        //$this->ProjectModal->setEmpId($this->input->post('emp_id'));
        $userId = $this->input->post('emp_id');
        $empTypeData = $this->getEmpName($userId);
        if($empTypeData[0]->emp_type == "admin"){
            $projectData = $this->dbresults->get_data('tbl_projects','*',array("proj_id"=>$this->input->post('proj_id')));
        }
        else{
            $projectData = $this->dbresults->get_data('tbl_projects','proj_id,projectNumber,proj_create_emp,
            proj_name,proj_type,proj_status,proj_manager,proj_developer,proj_start_date,proj_dead_line,
            proj_closing_date,proj_privlege,proj_created_at,proj_updated_at',array("proj_id"=>$this->input->post('proj_id')));
            if($projectData){
                $userArrayData = array();
                for($m=0; $m < count($projectData); $m++){
                    $proj_developer_id = $projectData[$m]->proj_developer;
                    $proj_manager_id = $projectData[$m]->proj_manager;
                    $proj_developer_id = explode(",", $proj_developer_id);
                    if(in_array($userId, $proj_developer_id)){
                        $userArrayData[] = $projectData[$m];
                    }
                    else if ($proj_manager_id == $userId){
                        $userArrayData[] = $projectData[$m];
                    }
                    $projectData = $userArrayData;
                }
            }
        }
        if($projectData) {
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Data Found Successfully';
            $response['emp_type'] = $empTypeData[0]->emp_type;
            for ($k = 0; $k < count($projectData); $k++) {
                $projectTeam = array();
                $proj_type_name = $this->getTypeName($projectData[$k]->proj_type);
                $proj_manager_name = $this->getEmpName($projectData[$k]->proj_manager);
                if($proj_manager_name != ""){
                    $proj_manager_name = $proj_manager_name[0]->emp_name;
                }
                if($proj_type_name != ""){
                    $proj_type_name = $proj_type_name[0]->type_name;
                }
                $projectData[$k]->proj_type_name = $proj_type_name;
                $projectData[$k]->proj_manager_name = $proj_manager_name;

                $proj_developer_name = $projectData[$k]->proj_developer;
                if (strpos($proj_developer_name, ',') !== false) {
                    $proj_developer_name = explode(",",$proj_developer_name);
                    for($i = 0; $i < count($proj_developer_name); $i++){
                        $projectName = $this->getEmpName($proj_developer_name[$i]);
                        if($projectName != ""){
                            $projectTeam[] = $projectName[0];
                        }
                    }
                    $projectData[$k]->projectTeam=$projectTeam;
                }
                else{
                    $projectName = $this->getEmpName($proj_developer_name);
                    if($projectName != ""){
                        $projectTeam[] = $projectName[0];
                    }
                    $projectData[$k]->projectTeam=$projectTeam;
                }
            }
            $response['projectData'] = $projectData;
        }
        else{
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Error in data found';
        }
        $this->validator->apiResponse($response);
    }
    public function getAllProjects(){
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $userId = $this->input->post('emp_id');
        $empTypeData = $this->getEmpName($userId);
        $employeeType = $empTypeData[0]->emp_type;
        $orderBy = array("proj_id" => "DESC");
        if($employeeType == "admin"){
            $where = array("status"=>"active");
            if($this->input->post('status_value') != ""){
                if($this->input->post('status_value') != "all"){
                    $where = array("status"=>"active","proj_status"=>$this->input->post('status_value'));
                }

            }
            $projectData = $this->dbresults->get_data('tbl_projects','*',$where,$orderBy);
        }
        else{
            $where = array("status"=>"active");
            if($this->input->post('status_value') != ""){
                if($this->input->post('status_value') != "all"){
                    $where = array("status"=>"active","proj_status"=>$this->input->post('status_value'));
                }
            }
            $projectData = $this->dbresults->get_data('tbl_projects','proj_id,projectNumber,proj_create_emp,
            proj_name,proj_type,proj_status,proj_manager,proj_developer,proj_start_date,proj_dead_line,
            proj_closing_date,proj_privlege,proj_created_at,proj_updated_at',$where,$orderBy);
            if($projectData){
                $userArrayData = array();
                for($m = 0; $m < count($projectData); $m++){
                    $proj_developer_id = $projectData[$m]->proj_developer;
                    $proj_manager_id = $projectData[$m]->proj_manager;
                    if($proj_developer_id != ""){
                        if(strpos($proj_developer_id,",") > 0){
                            $proj_developer_id = explode(",", $proj_developer_id);

                            if(in_array($proj_manager_id, $proj_developer_id)){
                               /* if($proj_manager_id == $userId){
                                    $userArrayData[] = $projectData[$m];
                                }
                                echo "<pre>";
                                    print_r($proj_developer_id);
                                echo "</pre>";*/
                                for($p = 0; $p < count($proj_developer_id); $p++ ){
                                    //echo "proj_developer_id > ".$proj_developer_id[$p]." proj_manager_id> ".$proj_manager_id." userId> ".$userId;
                                    //if($proj_manager_id != $proj_developer_id[$p]){
                                        /*if($proj_developer_id[$p] == $userId){
                                            $userArrayData[] = $projectData[$m];
                                        }
                                        if($proj_manager_id == $userId){
                                            $userArrayData[] = $projectData[$m];
                                        }*/
                                        if($proj_developer_id[$p] == $proj_manager_id){
                                            if($proj_developer_id[$p] == $userId){
                                                $userArrayData[] = $projectData[$m];
                                            }
                                        }
                                        else{
                                            if($proj_developer_id[$p] == $userId){
                                                $userArrayData[] = $projectData[$m];
                                            }
                                        }
                                    //}
                                }
                            }
                            else{
                                if($proj_manager_id == $userId){
                                    $userArrayData[] = $projectData[$m];
                                }
                                else{
                                    if(in_array($userId, $proj_developer_id)){
                                        $userArrayData[] = $projectData[$m];
                                    }
                                }
                            }
                        }
                        else{
                            if($proj_developer_id ==  $proj_manager_id){
                                if($proj_developer_id == $userId){
                                    $userArrayData[] = $projectData[$m];
                                }
                            }
                            else{
                                if($proj_developer_id == $userId){
                                    $userArrayData[] = $projectData[$m];
                                }
                                else if($proj_manager_id == $userId){
                                    $userArrayData[] = $projectData[$m];
                                }
                            }
                        }
                    }
                    else{
                        if($proj_manager_id == $userId){
                            $userArrayData[] = $projectData[$m];
                        }
                    }
                }
                $projectData = $userArrayData;
            }
        }
        if($projectData) {
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Data Found Successfully';
            for ($k = 0; $k < count($projectData); $k++) {
                $projectTeam = array();
                $projectStatus = $this->getProjStatus($projectData[$k]->proj_status);
                if($projectStatus != ""){
                    $status_name = $projectStatus[0]->status_title;
                }
                $proj_type_name = $this->getTypeName($projectData[$k]->proj_type);

                $proj_manager_name = $this->getEmpName($projectData[$k]->proj_manager);
                if($proj_manager_name != ""){
                    $proj_manager_name = $proj_manager_name[0]->emp_name;
                }
                if($proj_type_name != ""){
                    $proj_type_name = $proj_type_name[0]->type_name;
                }
                $projectData[$k]->proj_type_name = $proj_type_name;
                $projectData[$k]->proj_manager_name = $proj_manager_name;
                $projectData[$k]->status_name = $status_name;

                  $proj_developer_name = $projectData[$k]->proj_developer;
                  if (strpos($proj_developer_name, ',') !== false) {
                      $proj_developer_name = explode(",",$proj_developer_name);
                      for($i = 0; $i < count($proj_developer_name); $i++){
                          $projectName = $this->getEmpName($proj_developer_name[$i]);
                          if($projectName != ""){
                              $projectTeam[] = $projectName[0];
                          }
                      }
                      $projectData[$k]->projectTeam=$projectTeam;
                  }
                  else{
                      $projectName = $this->getEmpName($proj_developer_name);
                      if($projectName != ""){
                          $projectTeam[] = $projectName[0];
                      }
                      $projectData[$k]->projectTeam=$projectTeam;
                  }
            }
            $response['emp_type'] = $employeeType;
            $response['AllProject'] = $projectData;
        }
        else{
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Error in data found';
            $response['emp_type'] = $employeeType;
        }
        $this->validator->apiResponse($response);
    }
    public function getEmpName($empId){
        $empName = $this->dbresults->get_data('tbl_employees','emp_id,emp_name,emp_type',array("emp_id"=>$empId));
        if($empName){
            return $empName;
        }
        return "";
    }
    public function getProjStatus($statusId){
        $statusData = $this->dbresults->get_data('tbl_project_status` ','status_id,status_title',array("status_id"=>$statusId));
        if($statusData){
            return $statusData;
        }
        return "";
    }
    public function getTypeName($typeId){
        $typeName = $this->dbresults->get_data('tbl_project_type','type_id,type_name,type_status',array("type_id"=>$typeId));
        if($typeName){
            return $typeName;
        }
        return "";
    }
    /*get project type and employee data*/
    public function get_project_employee(){
        $projectTypeData = $this->ProjectModal->getProjectType();
        $employeeData = $this->ProjectModal->getEmployee();
        if($projectTypeData){
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Data Found Successfully';
            $response['allProjectType'] = $projectTypeData;
            $response['allEmployee'] = $employeeData;
        }
        else{
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Error in data found';
        }
        $this->validator->apiResponse($response);
    }
    public function createProject(){
        $requiredfields = array('projectName','projectType','projectManager','projectLeader','projectTeam','startDate',
            'deadlineDate','closingDate','currentDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjName($this->input->post('projectName'));
        $this->ProjectModal->setProjType($this->input->post('projectType'));
        $this->ProjectModal->setProjManager($this->input->post('projectManager'));
        $this->ProjectModal->setProjTeamLeader($this->input->post('projectLeader'));
        $this->ProjectModal->setProjDeveloper($this->input->post('projectTeam'));
        $this->ProjectModal->setProjStartDate($this->input->post('startDate'));
        $this->ProjectModal->setProjDeadLine($this->input->post('deadlineDate'));
        $this->ProjectModal->setProjClosingDate($this->input->post('closingDate'));
        $this->ProjectModal->setCurrentDate($this->input->post('currentDate'));
        $isCreate = $this->ProjectModal->createProject();
        $status = $isCreate['Status'];
        if(!$status) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = $isCreate['Message'];
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = $isCreate['Message'];
        $response['proj_id'] = $isCreate['proj_id'];
        $this->validator->apiResponse($response);
        return true;
    }
    public function updateProject(){
        $requiredfields = array('projectName','projectType','projectManager','projectLeader','projectTeam','startDate',
            'deadlineDate','closingDate','proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjName($this->input->post('projectName'));
        $this->ProjectModal->setProjType($this->input->post('projectType'));
        $this->ProjectModal->setProjManager($this->input->post('projectManager'));
        $this->ProjectModal->setProjTeamLeader($this->input->post('projectLeader'));
        $this->ProjectModal->setProjDeveloper($this->input->post('projectTeam'));
        $this->ProjectModal->setProjStartDate($this->input->post('startDate'));
        $this->ProjectModal->setProjDeadLine($this->input->post('deadlineDate'));
        $this->ProjectModal->setProjClosingDate($this->input->post('closingDate'));
        $this->ProjectModal->setProjId($this->input->post('proj_id'));
        $isUpdate = $this->ProjectModal->updateProject();
        if(!$isUpdate) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = "error in update project";
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = "project updated successfully";
        $this->validator->apiResponse($response);
        return true;
    }
    public function deleteProject(){
        $requiredfields = array('proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjId($this->input->post('proj_id'));
        $isDelete = $this->ProjectModal->deleteProject();
        if(!$isDelete) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = "error in delete project";
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = "Project delete successfully.";
        $this->validator->apiResponse($response);

        $dataComment = array('comm_status' => "delete");
        $whereComment = array('comm_proj_id'=>$this->input->post('proj_id'));

        $dataMilestone  = array('mile_status'=>"delete");
        $whereMilestone = array('mile_project_id'=>$this->input->post('proj_id'));
        $update_comment = $this->dbresults->update_data('tbl_comments',$dataComment,$whereComment);
        $update_milestone = $this->dbresults->update_data('tbl_milestone',$dataMilestone,$whereMilestone);
        return true;
    }
}
