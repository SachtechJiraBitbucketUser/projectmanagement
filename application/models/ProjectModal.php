<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/26/2017
 * Time: 7:03 PM
 */
class ProjectModal extends CI_Model
{
    private $result_array = array();
    private $response = array();
    private $proj_id = null;
    private $proj_type = null;
    private $proj_name = null;
    private $proj_status = null;
    private $proj_manager = null;
    private $proj_developer = null;
    private $comm_text = null;
    private $proj_start_date = null;
    private $proj_dead_line = null;
    private $proj_closing_date = null;
    private $proj_amount_type = null;
    private $mile_value = null;
    private $proj_total_amount = null;
    private $proj_total_emp = null;
    private $proj_hourly_rate = null;
    private $currentDate = null;
    private $proj_team_leader = null;
    private $emp_id = null;
    public function __construct()
    {
        parent::__construct();
    }
    function getProjMaxNum() {
        $this -> db ->select('proj_id');;
        $this -> db -> from('tbl_projects');
        $this -> db -> where('status',"active");
        $query = $this -> db -> get();
        return $query->num_rows();
    }
    function getProjects(){
        $this->db->select("tbl_projects.proj_id,tbl_projects.proj_name,tbl_projects.proj_type,tbl_projects.proj_manager
        ,tbl_projects.proj_developer,tbl_projects.proj_start_date,tbl_projects.proj_dead_line,
        tbl_projects.proj_closing_date,tbl_projects.proj_created_at,tbl_projects.proj_updated_at,tbl_project_type.type_id,
        tbl_project_type.type_name,type_status,tbl_project_type.type_desc");
        $this->db->from("tbl_projects");
        $this->db->join("tbl_project_type","tbl_projects.proj_type=tbl_project_type.type_id");
        $query = $this->db->get();
        if($query){
            $emp_id = $this->setEmpId;
            $checkUserType = $this->checkUserType($emp_id);
            if($checkUserType){
                return  $query->result();
            }
            else{
                //return  $query->result();
            }
        }
        else{
            return  false;
        }
    }
    function getProjectType(){
        $this->db->select("*");
        $this->db->from("tbl_project_type");
        $query = $this->db->get();
        if($query){
            return  $query->result();
        }
        else{
            return  false;
        }
    }
    function getEmployee(){
        $this->db->select("*");
        $this->db->from("tbl_employees");
        $query = $this->db->get();
        if($query){
            return  $query->result();
        }
        else{
            return  false;
        }
    }
    function createProject(){
        $maxVal = $this->getProjMaxNum();
        if($maxVal){
            $maxVal =  $maxVal + 1;
        }
        else{
            $maxVal = 1;
        }
        if($this->projectExist()){
            $projData = array(
                'proj_name' => $this->proj_name,
                'proj_create_emp' => $this->emp_id,
                'projectNumber' => $maxVal,
                'proj_type' => $this->proj_type,
                'proj_status ' => $this->proj_status,
                'proj_manager'=> $this->proj_manager,
                'proj_developer'  => $this->proj_developer,
                'proj_start_date'  => $this->proj_start_date,
                'proj_dead_line'  => $this->proj_dead_line,
                'proj_closing_date'  => $this->proj_closing_date,
                'proj_privlege'  => '1',
                'proj_created_at'  => $this->currentDate,
                'status'  => "active"
            );
            if($this->proj_amount_type != ""){
                if($this->proj_amount_type == "fixedPrice"){
                    $projData = $this->array_push_assoc($projData, 'proj_total_amount', $this->proj_total_amount);
                }
                else if($this->proj_amount_type == "timeMaterial"){
                    $projData =  $this->array_push_assoc($projData, 'proj_hourly_rate', $this->proj_hourly_rate);
                    $projData = $this->array_push_assoc($projData, 'proj_total_emp', $this->proj_total_emp);
                    $projData = $this->array_push_assoc($projData, 'proj_total_amount', $this->proj_total_amount);
                }
                $projData = $this->array_push_assoc($projData, 'proj_amount_type', $this->proj_amount_type);
            }
            $this->db->insert('tbl_projects', $projData);
            $proj_id = $this->db->insert_id();
            if($this->comm_text != ""){
                $commentData = array(
                    'comm_proj_id' => $proj_id,
                    'comm_text'  => $this->comm_text,
                    'comm_emp_id'  => $this->emp_id,
                    'comm_status'  => "active",
                    'comm_created_at'  => $this->currentDate
                );
                $this->db->insert('tbl_comments', $commentData);
            }
            if(count($this->mile_value) > 0){
                $milestoneArray =$this->mile_value;
                $j = 1;
                for($s = 0; $s < count($milestoneArray); $s++){
                    $j = $j + $s;
                    $mile_name  = "Milestone ".$j;
                    $mileData = array(
                        'mile_project_id'=> $proj_id,
                        'mile_name'  => $mile_name,
                        'mile_emp_id'  => $this->emp_id,
                        'mile_description'  => $milestoneArray[$s],
                        'mile_status'  => "active",
                        'mile_created_at'  => $this->currentDate
                    );
                    if($milestoneArray[$s] != ""){
                        $this->db->insert('tbl_milestone', $mileData);
                    }

                    $mileData = "";
                }
            }
            $result_array[$this->config->item('status')] = true;
            $result_array[$this->config->item('message')] = "Project successfully created";
            $result_array['proj_id'] = $proj_id;
            return $result_array;
        }
        else{
            $result_array[$this->config->item('status')] = false;
            $result_array[$this->config->item('message')] = "Project name allready exist";
            return $result_array;
        }
    }

    function array_push_assoc($array, $key, $value){
        $array[$key] = $value;
        return $array;
    }
    function updateProject(){
        if($this->projectExistUpdate()){
            $projData = array(
                'proj_name' => $this->proj_name,
                'proj_create_emp' => $this->emp_id,
                'proj_type' => $this->proj_type,
                'proj_status ' => $this->proj_status,
                'proj_manager'=> $this->proj_manager,
                'proj_developer'  => $this->proj_developer,
                'proj_start_date'  => $this->proj_start_date,
                'proj_dead_line'  => $this->proj_dead_line,
                'proj_closing_date'  => $this->proj_closing_date,
                'proj_created_at'  => $this->currentDate
            );
            if($this->proj_amount_type != ""){
                if($this->proj_amount_type == "fixedPrice"){
                    $projData = $this->array_push_assoc($projData, 'proj_total_amount', $this->proj_total_amount);
                }
                else if($this->proj_amount_type == "timeMaterial"){
                    $projData =  $this->array_push_assoc($projData, 'proj_hourly_rate', $this->proj_hourly_rate);
                    $projData = $this->array_push_assoc($projData, 'proj_total_emp', $this->proj_total_emp);
                    $projData = $this->array_push_assoc($projData, 'proj_total_amount', $this->proj_total_amount);
                }
                $projData = $this->array_push_assoc($projData, 'proj_amount_type', $this->proj_amount_type);
            }
            $this->db->where('proj_id',$this->proj_id);
            $this->db->update('tbl_projects', $projData);
            $afftectedRows = $this->db->affected_rows();
            if($afftectedRows>0) {
                $result_array[$this->config->item('status')] = true;
                $result_array[$this->config->item('message')] = "Project successfully updated";
                return $result_array;
            }
            $result_array[$this->config->item('status')] = false;
            $result_array[$this->config->item('message')] = "unable to update project";
            return $result_array;

        }
        else{
            $result_array[$this->config->item('status')] = false;
            $result_array[$this->config->item('message')] = "Project name allready exist";
            return $result_array;
        }
    }
    function projectExistUpdate(){
        $this->db->select("proj_name,proj_type");
        $this->db->from("tbl_projects");
        $this->db->where('proj_name', $this->proj_name);
        $this->db->where('proj_type',$this->proj_type );
        $this->db->where('proj_id !=',$this->proj_id);
        $this->db->where('status',"active");
        $query = $this->db->get();
        if ( $query->num_rows() > 0 )
        {
            $row = $query->row_array();
            return false;
        }
        return true;

    }
    function projectExist(){
        $this->db->select("proj_name,proj_type");
        $this->db->from("tbl_projects");
        $this->db->where('proj_name', $this->proj_name);
        $this->db->where('proj_type',$this->proj_type );
        $this->db->where('status',"active");
        $query = $this->db->get();
        if ( $query->num_rows() > 0 )
        {
            $row = $query->row_array();
            return false;
        }
        return true;

    }
    function deleteProject(){
        $projData = array(
            "status"=>"delete"
        );
        $this->db->where('proj_id',$this->proj_id);
        $this->db->update('tbl_projects', $projData);
        $afftectedRows = $this->db->affected_rows();
        if($afftectedRows>0) {
            return true;
        }
        return false;
    }
    function getProjectsEmployee(){
        $this->db->select("proj_id,proj_name,proj_type,proj_manager,proj_team_leader,proj_developer,proj_start_date,
                           proj_dead_line,proj_closing_date,proj_created_at,proj_updated_at");
        $this->db->from("tbl_projects");
        $query = $this->db->get();
        if($query){
            return  $query->result();
        }
        else{
            return  false;
        }
    }

    /**
     * @param null $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }
    /**
     * @param null $comm_text
     */
    public function setCommText($comm_text)
    {
        $this->comm_text = $comm_text;
    }

    /**
     * @param null $mile_value
     */
    public function setMileValue($mile_value)
    {
        $this->mile_value = $mile_value;
    }

    /**
     * @param null $proj_amount_type
     */
    public function setProjAmountType($proj_amount_type)
    {
        $this->proj_amount_type = $proj_amount_type;
    }

    /**
     * @param null $proj_hourly_rate
     */
    public function setProjHourlyRate($proj_hourly_rate)
    {
        $this->proj_hourly_rate = $proj_hourly_rate;
    }

    /**
     * @param null $proj_status
     */
    public function setProjStatus($proj_status)
    {
        $this->proj_status = $proj_status;
    }

    /**
     * @param null $proj_total_amount
     */
    public function setProjTotalAmount($proj_total_amount)
    {
        $this->proj_total_amount = $proj_total_amount;
    }

    /**
     * @param null $proj_total_emp
     */
    public function setProjTotalEmp($proj_total_emp)
    {
        $this->proj_total_emp = $proj_total_emp;
    }

    /**
     * @param null $currentDate
     */
    public function setCurrentDate($currentDate)
    {
        $this->currentDate = $currentDate;
    }
    /**
     * @param null $proj_id
     */
    public function setProjId($proj_id)
    {
        $this->proj_id = $proj_id;
    }

    /**
     * @param null $proj_name
     */
    public function setProjName($proj_name)
    {
        $this->proj_name = $proj_name;
    }

    /**
     * @param null $proj_closing_date
     */
    public function setProjClosingDate($proj_closing_date)
    {
        $this->proj_closing_date = $proj_closing_date;
    }

    /**
     * @param null $proj_dead_line
     */
    public function setProjDeadLine($proj_dead_line)
    {
        $this->proj_dead_line = $proj_dead_line;
    }

    /**
     * @param null $proj_developer
     */
    public function setProjDeveloper($proj_developer)
    {
        $this->proj_developer = $proj_developer;
    }

    /**
     * @param null $proj_manager
     */
    public function setProjManager($proj_manager)
    {
        $this->proj_manager = $proj_manager;
    }

    /**
     * @param null $proj_start_date
     */
    public function setProjStartDate($proj_start_date)
    {
        $this->proj_start_date = $proj_start_date;
    }

    /**
     * @param null $proj_team_leader
     */
    public function setProjTeamLeader($proj_team_leader)
    {
        $this->proj_team_leader = $proj_team_leader;
    }

    /**
     * @param null $proj_type
     */
    public function setProjType($proj_type)
    {
        $this->proj_type = $proj_type;
    }
}