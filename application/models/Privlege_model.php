<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 1/2/2018
 * Time: 9:17 AM
 */

class Privlege_model extends CI_Model
{
    private $response = array();
    public function __construct()
    {
        parent::__construct();
    }
    function getAllUsers() {
        $this -> db -> select('*');
        $this -> db -> from('tbl_employees');
        $this -> db -> where('emp_type !=', "admin");
        $query = $this -> db -> get();
        if($query -> num_rows() >0)
        {
            $response['Status'] = "Success";
            $response['Message'] = "Data found successfully";
            $response['UserData'] =  $query->result();
            return $response;
        }
        $response['Status'] = "Error";
        $response['Message'] = "Data not  found Successfully".$query;
        return $response;
    }
}