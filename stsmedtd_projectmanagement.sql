-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Feb 08, 2018 at 07:08 PM
-- Server version: 5.6.34-79.1-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stsmedtd_projectmanagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comments`
--

CREATE TABLE IF NOT EXISTS `tbl_comments` (
  `comm_id` int(100) NOT NULL AUTO_INCREMENT,
  `comm_text` text NOT NULL,
  `comm_proj_id` varchar(255) NOT NULL,
  `comm_emp_id` varchar(255) NOT NULL,
  `comm_status` varchar(255) NOT NULL,
  `comm_created_at` varchar(255) NOT NULL,
  `comm_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`comm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=150 ;

--
-- Dumping data for table `tbl_comments`
--

INSERT INTO `tbl_comments` (`comm_id`, `comm_text`, `comm_proj_id`, `comm_emp_id`, `comm_status`, `comm_created_at`, `comm_updated_at`) VALUES
(1, 'Closed', '1', '1', 'active', '12-01-2018', '2018-01-12 07:49:22'),
(2, '# Feb 8,2018\n Stylist app \n 1. Splash \n 2. Login page\n 3. Home Page With calendar \n\nUser App\nDynamic cats added (half)', '20', '22', 'active', '08-02-2018 07:26:29', '2018-02-08 13:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_designation`
--

CREATE TABLE IF NOT EXISTS `tbl_designation` (
  `desig_id` int(11) NOT NULL AUTO_INCREMENT,
  `desig_title` varchar(255) NOT NULL,
  `desi_created_at` varchar(255) NOT NULL,
  `desig_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`desig_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_designation`
--

INSERT INTO `tbl_designation` (`desig_id`, `desig_title`, `desi_created_at`, `desig_updated_at`) VALUES
(1, 'Android', '12-01-2018', '2018-01-12 06:40:47'),
(2, 'IOS', '12-01-2018', '2018-01-12 07:11:33'),
(3, 'PHP', '12-01-2018', '2018-01-12 07:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employees`
--

CREATE TABLE IF NOT EXISTS `tbl_employees` (
  `emp_id` int(100) NOT NULL AUTO_INCREMENT,
  `emp_name` varchar(255) NOT NULL,
  `emp_designation` varchar(255) NOT NULL,
  `emp_created_at` varchar(255) NOT NULL,
  `emp_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `emp_status` varchar(255) NOT NULL,
  `emp_email` varchar(255) NOT NULL,
  `emp_password` text NOT NULL,
  `emp_mobile` varchar(255) NOT NULL,
  `emp_message` text NOT NULL,
  `emp_img` varchar(255) NOT NULL,
  `emp_type` varchar(255) NOT NULL,
  `emp_otp` varchar(255) NOT NULL,
  `emp_privlege` text NOT NULL,
  `emp_menu_privlege` text NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `tbl_employees`
--

INSERT INTO `tbl_employees` (`emp_id`, `emp_name`, `emp_designation`, `emp_created_at`, `emp_updated_at`, `emp_status`, `emp_email`, `emp_password`, `emp_mobile`, `emp_message`, `emp_img`, `emp_type`, `emp_otp`, `emp_privlege`, `emp_menu_privlege`) VALUES
(1, 'Admin', '1', '2018-01-04 12:14', '2018-01-17 07:33:42', 'A', 'admin@gmail.com', '25d55ad283aa400af464c76d713c07ad', '1234567', 'We must let go of the life we have planned, so as to accept the one that is waiting for us.', 'e842022f802c76609e4b4058a009bf49.JPG', 'admin', '', 'create_emp,delete_emp,edit_emp', '1,2,3,4,5,6,7'),
(17, 'Sourav Kumar', '3', '12-01-2018 09:55', '2018-01-30 10:45:21', 'A', 'nk3527@gmail.com', '399eb33f2cd4f0db2d123e1a7a056e85', '9041634625', 'dfddfdf', 'ebcee342b5098cf42b70b780e537fdc1.png', 'user', 'CW0Rj', '', '1,2'),
(18, 'Harwinder Kumar', '3', '12-01-2018 09:58', '2018-01-23 20:37:15', 'A', 'harwinderkumar820@gmail.com', '8e48ff63cb74647d884cfce76ca3e895', '9779784701', 'I''m selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can''t handle me at my worst, then you sure as hell don''t deserve me at my best.', '522e053820b2e209436472811ca8c15b.jpg', 'user', 'pUe6D', '', '1,2'),
(19, 'Ravikant Sharma', '3', '12-01-2018 09:59', '2018-01-22 13:42:46', 'A', 'raviksharma9021@gmail.com', '1d3271c9402290994e04b745b4813c7f', '9988225098', '', 'af55364abc960964a5c466d499c0d1c5.png', 'user', 'Tqzac', '', '1,2'),
(20, 'Ashish Bhardwaj', '1', '12-01-2018 10:02', '2018-01-16 11:58:39', 'A', 'ashishbhardwaj61@gmail.com', '1f9daf7c77ee9e20919b22a29dbc736b', '', '', '', 'user', 'kLYGJ', '', '1,2'),
(21, 'Amandeep Kaur', '1', '12-01-2018 10:03', '2018-01-16 12:01:36', 'A', 'deoaman123@gmail.com', '65a3d000622ea6f8489af12652cf1adb', '', '', '', 'user', 'VHIUv', '', '1,2'),
(22, 'Avtar Singh', '2', '12-01-2018 10:04', '2018-01-25 04:52:01', 'A', 'dhaliwal.avtar1@gmail.com', '21cbe0d2fac6a27bdbd80dab5fc6d463', '9592394097', 'Be Happy always\nThink Different ', 'f6c1b8ba0eeff6bf8ba07032ad4a4615.jpg', 'user', 'Nj74D', '', '1,2'),
(23, 'Chanpreet Singh', '2', '12-01-2018 10:07', '2018-02-05 05:13:19', 'A', 'rupalchanpreet@gmail.com', '4528cdeba8cf4bb0cfca5809eba8b04b', '', '', 'b8a1be48cc5f7f3fc144e0547fac306d.png', 'user', 'UE3pW', '', '1,2'),
(24, 'Gurtek singh', '1', '12-01-2018 10:08', '2018-01-16 11:59:06', 'A', 'gurtek@protonmail.ch', 'e10adc3949ba59abbe56e057f20f883e', '9779377789', '', '', 'user', '', '', '1,2'),
(25, 'Nardeep Sandhu', '1', '12-01-2018 10:09', '2018-01-12 13:00:53', 'A', 'nardeepsandhu5@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 'user', '', '', '1,2'),
(26, 'Akash Saggu', '1', '12-01-2018 10:11', '2018-01-20 19:33:45', 'A', 'akashsaggu96@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9888343111', '', 'c8695767a32be52fe59ad490cbe8ac76.png', 'user', 'POAwI', '', '1,2'),
(27, 'Jai Kumar', '1', '12-01-2018 10:16', '2018-01-16 11:57:32', 'A', 'jai.sachtech@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '9914483338', 'Do your best  rest left on Client because Upar wala sab dekh rha hai', '', 'user', '', '', '1,2'),
(28, 'Manish Aarya', '1', '12-01-2018 10:18', '2018-02-03 04:09:33', 'A', 'manishpreet0186@gmail.com', '17de6b857063759d22198c58ba0c7c25', '9478586699', 'I want to do something great', 'f1d53f5c03935f9b409965191d292552.png', 'user', '0HhVF', '', '1,2');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(255) NOT NULL,
  `menu_page` varchar(255) NOT NULL,
  `menu_status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id`, `menu_name`, `menu_page`, `menu_status`) VALUES
(1, 'Dashboard', '', '1'),
(2, 'Projects', 'project/1', '1'),
(3, 'Employees', 'user/users', '1'),
(4, 'Type', 'projecttypee', '1'),
(5, 'Status', 'status', '1'),
(6, 'Privilege', 'privlege', '1'),
(7, 'Designation', 'designation', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_milestone`
--

CREATE TABLE IF NOT EXISTS `tbl_milestone` (
  `mile_id` int(100) NOT NULL AUTO_INCREMENT,
  `mile_project_id` varchar(255) NOT NULL,
  `mile_emp_id` varchar(255) NOT NULL,
  `mile_description` text NOT NULL,
  `mile_name` varchar(255) NOT NULL,
  `mile_start_date` varchar(255) NOT NULL,
  `mile_end_date` varchar(255) NOT NULL,
  `mile_status` varchar(255) NOT NULL,
  `mile_created_at` varchar(255) NOT NULL,
  `mile_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mile_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `tbl_milestone`
--

INSERT INTO `tbl_milestone` (`mile_id`, `mile_project_id`, `mile_emp_id`, `mile_description`, `mile_name`, `mile_start_date`, `mile_end_date`, `mile_status`, `mile_created_at`, `mile_updated_at`) VALUES
(1, '1', '1', 'Mile 1', 'Milestone 1', '', '', 'active', '12-01-2018', '2018-01-12 07:49:22'),
(2, '20', '1', 'o	Full end to end testing by customer, users.\no	Apps to go live on both Google play store and Apple Store.\no	You will get your apps working both in English and Arabic languages.\no	Source code to be appropriately indexed and documented, and to be delivered.', 'Milestone 6', '21/Mar/2018', '28/Mar/2018', 'active', '29-01-2018 01:46:41', '2018-01-29 08:16:32');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_privlege`
--

CREATE TABLE IF NOT EXISTS `tbl_privlege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(255) NOT NULL,
  `privlegeValue` varchar(255) NOT NULL,
  `created_at` varchar(100) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_privlege`
--

INSERT INTO `tbl_privlege` (`id`, `emp_id`, `privlegeValue`, `created_at`, `updated_on`) VALUES
(1, '1', '1,2,3', '12-1-2017', '2018-01-02 06:18:44'),
(2, '2', '2,3', '10-12-2018', '2018-01-02 09:56:54'),
(3, '4', '1,2,5', '02-01-2018', '2018-01-02 09:46:50');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_projects`
--

CREATE TABLE IF NOT EXISTS `tbl_projects` (
  `proj_id` int(100) NOT NULL AUTO_INCREMENT,
  `projectNumber` int(11) NOT NULL,
  `proj_create_emp` varchar(255) NOT NULL,
  `proj_name` varchar(50) NOT NULL,
  `proj_type` varchar(255) NOT NULL,
  `proj_status` varchar(255) NOT NULL,
  `proj_manager` varchar(255) NOT NULL,
  `proj_developer` varchar(255) NOT NULL,
  `proj_start_date` varchar(255) NOT NULL,
  `proj_dead_line` varchar(255) NOT NULL,
  `proj_closing_date` varchar(255) NOT NULL,
  `proj_amount_type` varchar(255) NOT NULL,
  `proj_total_amount` varchar(255) NOT NULL,
  `proj_hourly_rate` varchar(255) NOT NULL,
  `proj_total_emp` varchar(255) NOT NULL,
  `proj_privlege` text NOT NULL,
  `status` varchar(255) NOT NULL,
  `proj_created_at` varchar(255) NOT NULL,
  `proj_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`proj_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_projects`
--

INSERT INTO `tbl_projects` (`proj_id`, `projectNumber`, `proj_create_emp`, `proj_name`, `proj_type`, `proj_status`, `proj_manager`, `proj_developer`, `proj_start_date`, `proj_dead_line`, `proj_closing_date`, `proj_amount_type`, `proj_total_amount`, `proj_hourly_rate`, `proj_total_emp`, `proj_privlege`, `status`, `proj_created_at`, `proj_updated_at`) VALUES
(1, 1, '1', 'Calidad Android App', '7', '10', '24', '24', '11-01-2018', '31-01-2018', '', 'timeMaterial', '', '', '', '1', 'active', '', '2018-01-29 07:42:20'),
(2, 2, '1', 'PDF Reader IPhone App', '8', '10', '22', '22', '08-02-2018', '23-02-2018', '', 'fixedPrice', '900$', '', '', '1', 'active', '06-02-2018', '2018-02-06 08:11:48');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_status`
--

CREATE TABLE IF NOT EXISTS `tbl_project_status` (
  `status_id` int(100) NOT NULL AUTO_INCREMENT,
  `status_title` text NOT NULL,
  `status_created_at` varchar(255) NOT NULL,
  `status_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `tbl_project_status`
--

INSERT INTO `tbl_project_status` (`status_id`, `status_title`, `status_created_at`, `status_updated_at`) VALUES
(5, 'New', '04-01-2018', '2018-01-08 12:32:49'),
(6, 'Live (Stabilization)', '04-01-2018', '2018-01-04 10:54:40'),
(7, 'Live (Closed)', '04-01-2018', '2018-01-04 10:55:11'),
(8, 'On Hold', '04-01-2018', '2018-01-04 10:55:25'),
(9, 'Archived', '04-01-2018', '2018-01-04 10:56:37'),
(10, 'Assigned', '08-01-2018', '2018-01-08 12:32:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_project_type`
--

CREATE TABLE IF NOT EXISTS `tbl_project_type` (
  `type_id` int(100) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) NOT NULL,
  `type_status` varchar(255) NOT NULL,
  `type_desc` text NOT NULL,
  `type_created_at` varchar(255) NOT NULL,
  `type_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `tbl_project_type`
--

INSERT INTO `tbl_project_type` (`type_id`, `type_name`, `type_status`, `type_desc`, `type_created_at`, `type_updated_at`) VALUES
(1, 'PHP', 'A', 'php developer', '28-12-2017', '2018-01-04 14:02:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
